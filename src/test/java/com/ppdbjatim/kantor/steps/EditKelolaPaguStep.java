package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.PaguData;
import com.ppdbjatim.kantor.pages.EditKelolaPaguPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class EditKelolaPaguStep extends ScenarioSteps {
    private EditKelolaPaguPage editKelolaPaguPage;

    @Then("^user should be in Edit Pagu SMA page$")
    public void userShouldBeInEditPaguSMAPage() {
        assertThat("Title in edit pagu sma page is wrong", editKelolaPaguPage.getKelolaPageSMASubTitle(),
                containsString("Edit Pagu SMA"));
    }

    @When("^user change rombel in (\\d+)th class$")
    public void userChangeRombelInThClass(int level) {
        String newRombel;

        if (level == 12)
        {
            newRombel = editKelolaPaguPage.checkAndChangeRombel(12);
            editKelolaPaguPage.editRombelClass12(newRombel);
        }

        else if (level == 11)
        {
            newRombel = editKelolaPaguPage.checkAndChangeRombel(11);
            editKelolaPaguPage.editRombelClass11(newRombel);
        }

        else if (level == 10)
        {
            newRombel = editKelolaPaguPage.checkAndChangeRombel(10);
            editKelolaPaguPage.editRombelClass10(newRombel);
        }
    }

    @And("^user change total siswa rombel$")
    public void userChangeTotalSiswaRombel() {
        String newSiswaRombel;
        newSiswaRombel = editKelolaPaguPage.checkAndChangeSiswaRombel();
        editKelolaPaguPage.editSiswaRombel(newSiswaRombel);
    }

    @And("^set data rombel and total siswa$")
    public void setDataRombelAndTotalSiswa() {
        String rombelClass12 = editKelolaPaguPage.getRombelClass12();
        String rombelClass11 = editKelolaPaguPage.getRombelClass11();
        String rombelClass10 = editKelolaPaguPage.getRombelClass10();
        String siswaPerRombel = editKelolaPaguPage.getsiswaPerRombel();

        PaguData.getInstance().setRombelClass12(rombelClass12);
        PaguData.getInstance().setRombelClass11(rombelClass11);
        PaguData.getInstance().setRombelClass10(rombelClass10);
        PaguData.getInstance().setSiswaPerRombel(siswaPerRombel);
    }

    @And("^user click save edit pagu button$")
    public void userClickSaveEditPaguButton() {
        editKelolaPaguPage.clickBtnSaveEditPagu();
    }

    @And("^user click cancel edit pagu button$")
    public void userClickCancelEditPaguButton() {
        editKelolaPaguPage.clickBtnCancelEditPagu();
    }

    @When("^user change total siswa rombel into (.*)$")
    public void userChangeTotalSiswaRombelInto(String totalRombel) {
        editKelolaPaguPage.editSiswaRombel(totalRombel);
    }

    @Then("^appear warning modal maximal siswa per rombel$")
    public void appearWarningModalMaximalSiswaPerRombel() {
        assertThat("Modal Warning Max Siswa Per Rombel is not displayed", editKelolaPaguPage.isModalWarningMaxSiswaPerRombelDisplayed(),
                equalTo(true));
    }
}
