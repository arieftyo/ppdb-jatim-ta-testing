package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.VerifikasiStudentData;
import com.ppdbjatim.kantor.pages.CommonPage;
import com.ppdbjatim.kantor.pages.DaftarSelesaiVerifikasiPage;
import com.ppdbjatim.kantor.sql.SQLUpateSiswaVerification;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DaftarSelesaiVerifikasiStep extends ScenarioSteps {
    DaftarSelesaiVerifikasiPage daftarSelesaiVerifikasiPage;
    CommonPage commonPage;

    SQLUpateSiswaVerification sqlUpateSiswaVerification = new SQLUpateSiswaVerification();

    @And("^student data is showed in success verification page$")
    public void studentDataIsShowedInSuccessVerificationPage() {
        assertThat("Student is not in Success verification page",
                daftarSelesaiVerifikasiPage.isStudentInTableSuccessVerification(VerifikasiStudentData.getInstance().getNoUn()),
                equalTo(true));
    }

    @When("^user click menu daftar siswa selesai diverifikasi$")
    public void userClickMenuDaftarSiswaSelesaiDiverifikasi() {
        commonPage.clickBtnTogle();
        daftarSelesaiVerifikasiPage.clickMenuDaftarSelesaiVerifikasi();
    }

    @Then("^total student in daftar siswa selesai verifikasi must be true$")
    public void totalStudentInDaftarSiswaSelesaiVerifikasiMustBeTrue() {
        assertThat("Total students in selesai verifikasi is wrong", daftarSelesaiVerifikasiPage.getTotalSiswaInTabel(),
                equalTo(daftarSelesaiVerifikasiPage.getTotalSiswaShowed()));
    }

    @When("^user click last pagination table button$")
    public void userClickLastPaginationTableButton() {
        daftarSelesaiVerifikasiPage.clickBtnLastPagination();
    }
}
