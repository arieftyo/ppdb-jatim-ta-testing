package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.PaguData;
import com.ppdbjatim.kantor.pages.CommonPage;
import com.ppdbjatim.kantor.pages.KelolaPaguPage;
import com.ppdbjatim.kantor.properties.KelolaPaguProperties;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class KelolaPaguPageStep extends ScenarioSteps {
    private KelolaPaguPage kelolaPaguPage;
    CommonPage commonPage;

    PaguData paguData;

    @Given("^user click menu kelola pagu$")
    public void userClickMenuKelolaPagu() {
        commonPage.clickBtnTogle();
        kelolaPaguPage.clickMenuKelolaPagu();
    }

    @And("^user click menu kelola pagu (.*)$")
    public void userClickMenuKelolaPaguType(String type) {
        if (type.equalsIgnoreCase("Pagu SMA"))
            kelolaPaguPage.clickMenuPaguSMA();
        else if (type.equalsIgnoreCase("Pagu SMK"))
            kelolaPaguPage.clickMenuPaguSMK();
        else if (type.equalsIgnoreCase("Pagu SMA Tidak Naik"))
            kelolaPaguPage.clickMenuPaguSMATidakNaik();
        else if (type.equalsIgnoreCase("Pagu SMK Tidak Naik"))
            kelolaPaguPage.clickMenuPaguSMKTidakNaik();
    }

    @Then("^user should be in kelola pagu page (.*)$")
    public void userShouldBeInKelolaPaguPageType(String type) {
        String titlePage = kelolaPaguPage.getKelolaPaguTitle();
        if (type.equalsIgnoreCase("Pagu SMA"))
            assertThat("Title in kelola pagu SMA is wrong", titlePage.toLowerCase(),
                containsString("kelola pagu sma"));
        else if (type.equalsIgnoreCase("Pagu SMK"))
            assertThat("Title in kelola pagu SMK is wrong", titlePage.toLowerCase(),
                    containsString("kelola pagu smk"));
        else if (type.equalsIgnoreCase("Pagu SMA Tidak Naik"))
            assertThat("Title in kelola pagu SMA Tidak Naik is wrong", titlePage.toLowerCase(),
                    containsString("edit pagu sma tidak naik"));
        else if (type.equalsIgnoreCase("Pagu SMK Tidak Naik"))
            assertThat("Title in kelola pagu SMK Tidak Naik is wrong", titlePage.toLowerCase(),
                    containsString("edit pagu smk tidak naik"));
    }

    @When("^select one city in kelola pagu page$")
    public void selectOneCityInKelolaPaguPage() {
        kelolaPaguPage.clickSelectKotaKabupaten(KelolaPaguProperties.getInstance().getKotaKab());
    }

    @Then("^data kelola pagu page should be corresponding with the city$")
    public void dataKelolaPaguPageShouldBeCorrespondingWithTheCity() {
        assertThat("City in the title is wrong", kelolaPaguPage.getKelolaPaguTitle(),
                containsString(KelolaPaguProperties.getInstance().getKotaKab()));
        assertThat("City selected is wrong", kelolaPaguPage.getSelectedKotaKabupaten(),
                containsString(KelolaPaguProperties.getInstance().getKotaKab()));
    }

    @Then("^data pagu awal must be true$")
    public void dataPaguAwalMustBeTrue() {
        int dataRombelClass10 = Integer.parseInt(kelolaPaguPage.getFirstDataRombelClass10());
        int dataSiswaRombel = Integer.parseInt(kelolaPaguPage.getFirstDataSiswaRombel());
        String paguAwal = String.valueOf(dataRombelClass10 * dataSiswaRombel);
        assertThat("Data Pagu Awal is wrong", kelolaPaguPage.getDataPaguAwal(), equalTo(paguAwal));
    }

    @When("^user click edit pagu button$")
    public void userClickEditPaguButton() {
        kelolaPaguPage.clickButtonEditPaguInFirstSchool();
    }


    @Then("^rombel and data siswa must same with data in kelola page$")
    public void rombelAndDataSiswaMustSameWithDataInKelolaPage() {
       assertThat("Data rombel class 12 is wrong", PaguData.getInstance().getRombelClass12(),
               equalTo(kelolaPaguPage.getFirstDataRombelClass12()));
        assertThat("Data rombel class 11 is wrong", PaguData.getInstance().getRombelClass11(),
                equalTo(kelolaPaguPage.getFirstDataRombelClass11()));
        assertThat("Data rombel class 10 is wrong", PaguData.getInstance().getRombelClass10(),
                equalTo(kelolaPaguPage.getFirstDataRombelClass10()));
        assertThat("Data siswa per rombel is wrong", PaguData.getInstance().getSiswaPerRombel(),
                equalTo(kelolaPaguPage.getFirstDataSiswaRombel()));
    }

    @Then("^rombel and data siswa must different with data in kelola page$")
    public void rombelAndDataSiswaMustDifferentWithDataInKelolaPage() {
        assertThat("Data rombel class 12 is wrong", PaguData.getInstance().getRombelClass12(),
                not(kelolaPaguPage.getFirstDataRombelClass12()));
        assertThat("Data rombel class 11 is wrong", PaguData.getInstance().getRombelClass11(),
                not(kelolaPaguPage.getFirstDataRombelClass11()));
        assertThat("Data rombel class 10 is wrong", PaguData.getInstance().getRombelClass10(),
                not(kelolaPaguPage.getFirstDataRombelClass10()));
        assertThat("Data siswa per rombel is wrong", PaguData.getInstance().getSiswaPerRombel(),
                not(kelolaPaguPage.getFirstDataSiswaRombel()));
    }
}
