package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.PaguData;
import com.ppdbjatim.kantor.pages.KelolaPaguSMKPage;
import com.ppdbjatim.kantor.properties.KelolaPaguProperties;
import com.ppdbjatim.kantor.sql.SQLInsertJurusanSMK;
import com.ppdbjatim.kantor.sql.SQLUpdateJurusanSMK;
import com.ppdbjatim.kantor.sql.SQLUpdateSiswaHealthVerification;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class KelolaPaguSMKPageStep extends ScenarioSteps {
    private KelolaPaguSMKPage kelolaPaguSMKPage;

    SQLInsertJurusanSMK sqlInsertJurusanSMK = new SQLInsertJurusanSMK();
    SQLUpdateJurusanSMK sqlUpdateJurusanSMK = new SQLUpdateJurusanSMK();

    @When("^user choose one school SMK$")
    public void userChooseOneSchoolSMK() {
        kelolaPaguSMKPage.clickSelectFirstSchool();
    }

    @Then("^school that is choosen must be true$")
    public void schoolThatIsChoosenMustBeTrue() {
        List<String> dataSchoolName = kelolaPaguSMKPage.getSchoolDataName();

        for (int i =0; i<dataSchoolName.size();i++)
        {
            assertThat("School Name in table is wrong", dataSchoolName.get(i),
                    equalTo(kelolaPaguSMKPage.getSelectedSchool()));
        }

        assertThat("Title school is wrong", kelolaPaguSMKPage.getSchoolTitle(),
                containsString(kelolaPaguSMKPage.getSelectedSchool()));
    }

    @When("^user change rombel in (\\d+)th class SMK$")
    public void userChangeRombelInThClassSMK(int level) {
        String newRombel;

        if (level == 12)
        {
            newRombel = kelolaPaguSMKPage
                    .checkAndChangeSiswaRombelSMK(kelolaPaguSMKPage.getInputRombelClass12SMK());
            kelolaPaguSMKPage.editRombelClass12SMK(newRombel);
        }

        if (level == 11)
        {
            newRombel = kelolaPaguSMKPage
                    .checkAndChangeSiswaRombelSMK(kelolaPaguSMKPage.getInputRombelClass11SMK());
            kelolaPaguSMKPage.editRombelClass11SMK(newRombel);
        }
    }

    @And("^user click save edit pagu button SMK$")
    public void userClickSaveEditPaguButtonSMK() {
        kelolaPaguSMKPage.clickBtnSaveEditRombel();
    }

    @Then("^rombel must be same with edited data SMK$")
    public void rombelMustBeSameWithEditedDataSMK() {
        assertThat("Class Rombel 12 is wrong", kelolaPaguSMKPage.getRombelClass12(),
                equalTo(PaguData.getInstance().getRombelClass12()));
        assertThat("Class Rombel 11 is wrong", kelolaPaguSMKPage.getRombelClass11(),
                equalTo(PaguData.getInstance().getRombelClass11()));
    }

    @And("^total rombel must be true SMK$")
    public void totalRombelMustBeTrueSMK() {
        assertThat("Total Rombel is wrong", kelolaPaguSMKPage.getTotalRombelFromData(),
                equalTo(Integer.parseInt(kelolaPaguSMKPage.getTotalRombelClass())) );
    }

    @And("^set data rombel SMK$")
    public void setDataRombelSMK() {
        PaguData.getInstance().setRombelClass11(kelolaPaguSMKPage.getInputRombelClass11SMK());
        PaguData.getInstance().setRombelClass12(kelolaPaguSMKPage.getInputRombelClass12SMK());
    }

    @And("^user click edit pagu button SMK$")
    public void userClickEditPaguButtonSMK() {
        kelolaPaguSMKPage.clickBtnEditRombelClass();
    }

    @When("^user change rombel in twelfth class SMK into (.*)$")
    public void userChangeRombelInTwelfthClassSMKInto(String rombel) {
        kelolaPaguSMKPage.editRombelClass12SMK(rombel);
    }

    @Then("^appear warning modal maximal total rombel SMK$")
    public void appearWarningModalMaximalTotalRombelSMK() {
        assertThat("Warning Modal maximal rombel is not appeared",
                kelolaPaguSMKPage.isWarningMaximalRombelDisplayed(),
                equalTo(true));
    }

    @Then("^total rombel tenth class must be same with data SMK$")
    public void totalRombelTenthClassMustBeSameWithDataSMK() {
        assertThat("Total rombel in tenth class data table is not same with rombel data",
                kelolaPaguSMKPage.getTotalDataTableRombelTenthClass(),
                equalTo(Integer.parseInt(kelolaPaguSMKPage.getRombleClass10())));
    }

    @Then("^total all pagu awal tenth class must be true SMK$")
    public void totalAllPaguAwalTenthClassMustBeTrueSMK() {
        assertThat("Total Pagu Awal is wrong", kelolaPaguSMKPage.getTotalDataTablePaguAwal(),
                equalTo(kelolaPaguSMKPage.getTotalPaguAwal()));
    }

    @Then("^data pagu awal are got from multiplication rombel with total student SMK$")
    public void dataPaguAwalAreGotFromMultiplicationRombelWithTotalStudentSMK() {
        assertThat("Data pagu awal is wrong, not got from the multipiclation rombel and total student",
                kelolaPaguSMKPage.isDataPaguAwalTrue(), equalTo(true));
    }

    @And("^user delete one jurusan in SMK$")
    public void userDeleteOneJurusanInSMK() {
        int indexJurusan =  kelolaPaguSMKPage.getIndexJuruan(KelolaPaguProperties.getInstance().getDeleteJurusanSMK());
        PaguData.getInstance().setJurusan(kelolaPaguSMKPage.getJurusanByIndex(indexJurusan));
        kelolaPaguSMKPage.deleteJurusan(indexJurusan);
    }

    @Then("^jurusan SMK must be deleted$")
    public void jurusanSMKMustBeDeleted() {
        assertThat("Jurusan is not deleted", kelolaPaguSMKPage.getFirstJurusan(), not(equalTo(PaguData.getInstance().getJurusan())));
        assertThat("Success delete alert doesn't appear", kelolaPaguSMKPage.isSuccessAlertDisplayed(), equalTo(true));
        assertThat("Success delete alert message is wrong", kelolaPaguSMKPage.getSuccessAlert(), containsString("Sukses menghapus paket keahlian"));
    }

    @When("^delete jurusan if that exist$")
    public void deleteJurusanIfThatExist() {
        kelolaPaguSMKPage.deleteJurusanByJurusanName(KelolaPaguProperties.getInstance().getPaketKeahlianSMK());
    }

    @And("^user add jurusan in SMK$")
    public void userAddJurusanInSMK() {
        kelolaPaguSMKPage.clickBtnAddPaketKeahlian();
    }

    @Then("^jurusan should be added to SMK$")
    public void jurusanShouldBeAddedToSMK() {
        kelolaPaguSMKPage.isJurusanExist(KelolaPaguProperties.getInstance().getPaketKeahlianSMK());
    }

    @When("^user click edit button in first jurusan SMK$")
    public void userClickEditButtonInFirstJurusanSMK() {
        kelolaPaguSMKPage.clickFirstBtnEditJurusan();
    }

    @And("^user change tenth class rombel in edit jurusan SMK$")
    public void userChangeTenthClassRombelInEditJurusanSMK() {
        kelolaPaguSMKPage.editRombelClass10SMK(kelolaPaguSMKPage.checkAndChangeRombelSMK());
    }

    @And("^user change siswa per rombel in edit jurusan SMK$")
    public void userChangeSiswaPerRombelInEditJurusanSMK() {
        kelolaPaguSMKPage.editSiswaRombelJurusanSMK(kelolaPaguSMKPage.changeSiswaTenthClassRombelSMK());
    }

    @When("^set data tenth class rombel and siswa per rombel$")
    public void setDataTenthClassRombelAndSiswaPerRombel() {
        PaguData.getInstance().setRombelClass10(kelolaPaguSMKPage.getInputRombelClass10SMK());
        PaguData.getInstance().setSiswaPerRombel(kelolaPaguSMKPage.getInputSiswaPerRombelSMK());
    }

    @Then("^tenth class rombel and siswa per rombel must be changed$")
    public void tenthClassRombelAndSiswaPerRombelMustBeChanged() {
        assertThat("Tenth class rombel is not changed", kelolaPaguSMKPage.getDataFirstJurusanRombel(),
                equalTo(PaguData.getInstance().getRombelClass10()));

        assertThat("Siswa per rombel is not changed", kelolaPaguSMKPage.getDataFirstJurusanSiswaPerRombel(),
                equalTo(PaguData.getInstance().getSiswaPerRombel()));
    }

    @And("^user change siswa per rombel in edit jurusan SMK into the exceeded value$")
    public void userChangeSiswaPerRombelInEditJurusanSMKIntoTheExceededValue() {
        kelolaPaguSMKPage.editSiswaRombelJurusanSMK(KelolaPaguProperties.getInstance().getExceededRombel());
    }

    @And("^user select bidang keahlian SMK$")
    public void userSelectBidangKeahlianSMK() {
        kelolaPaguSMKPage.clickSelectBidangKeahlianByName(KelolaPaguProperties.getInstance().getBidangKeahlianSMK());
    }

    @And("^user select paket keahlian SMK$")
    public void userSelectPaketKeahlianSMK() {
        kelolaPaguSMKPage.clickSelectPaketKeahlianByName(KelolaPaguProperties.getInstance().getPaketKeahlianSMK());
    }

    @And("^user input rombel in add jurusan SMK$")
    public void userInputRombelInAddJurusanSMK() {
        kelolaPaguSMKPage.addJurusanRombel(KelolaPaguProperties.getInstance().getJurusanRombelSMK());
    }

    @And("^user input siswa per rombel in add jurusan SMK$")
    public void userInputSiswaPerRombelInAddJurusanSMK() {
        kelolaPaguSMKPage.addJurusanSiswaPerRombel(KelolaPaguProperties.getInstance().getJurusanSiswaSMK());
    }

    @And("^user click save add jurusan SMK$")
    public void userClickSaveAddJurusanSMK() {
        kelolaPaguSMKPage.clickSaveAddJurusan();
    }

    @And("^insert jurusan again$")
    public void insertJurusanAgain() {
        sqlInsertJurusanSMK.InsertJurusanIntoSMK1Surabaya();
        sqlUpdateJurusanSMK.UpdateJurusanIntoNormalSMK1Surabaya();
    }
}
