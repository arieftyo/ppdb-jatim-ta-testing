package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.PaguData;
import com.ppdbjatim.kantor.pages.KelolaPaguSMATidakNaikPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class KelolaPaguSMATidakNaikStep extends ScenarioSteps {
    KelolaPaguSMATidakNaikPage kelolaPaguSMATidakNaikPage;

    @When("^user click edit button in first school SMA Tidak Naik$")
    public void userClickEditButtonInFirstSchoolSMATidakNaik() {
        kelolaPaguSMATidakNaikPage.clickFirstSchoolEditPaguTidakNaik();
    }

    @And("^user change pagu in SMA Tidak Naik$")
    public void userChangePaguInSMATidakNaik() {
        kelolaPaguSMATidakNaikPage.changeFirstPaguTidakNaik(kelolaPaguSMATidakNaikPage.checkPaguTidakNaik());
    }

    @And("^set data pagu in SMA Tidak Naik$")
    public void setDataPaguInSMATidakNaik() {
        PaguData.getInstance().setPaguTidakNaik(kelolaPaguSMATidakNaikPage.getFirstInputPaguTidakNaik());
    }

    @Then("^data pagu tidak naik must be changed$")
    public void dataPaguTidakNaikMustBeChanged() {
        assertThat("Data pagu tidak naik is not changed", kelolaPaguSMATidakNaikPage.getFirstSchoolPaguTidakNaik(),
                equalTo(PaguData.getInstance().getPaguTidakNaik()));
    }

    @And("^user click save edit pagu SMA Tidak Naik$")
    public void userClickSaveEditPaguSMATidakNaik() {
        kelolaPaguSMATidakNaikPage.clickFirstBtnSsvePaguTidakNaik();
    }
}
