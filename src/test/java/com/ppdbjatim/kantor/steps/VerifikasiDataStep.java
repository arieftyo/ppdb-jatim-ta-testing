package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.VerifikasiStudentData;
import com.ppdbjatim.kantor.pages.CommonPage;
import com.ppdbjatim.kantor.pages.DaftarSelesaiVerifikasiPage;
import com.ppdbjatim.kantor.pages.VerifikasiDataDiriPage;
import com.ppdbjatim.kantor.pages.VerifikasiDataPage;
import com.ppdbjatim.kantor.properties.VerifikasiDataProperties;
import com.ppdbjatim.kantor.sql.SQLUpateSiswaVerification;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class VerifikasiDataStep extends ScenarioSteps {
    VerifikasiDataPage verifikasiDataPage;
    VerifikasiDataDiriPage verifikasiDataDiriPage;
    CommonPage commonPage;
    DaftarSelesaiVerifikasiPage daftarSelesaiVerifikasiPage;
    SQLUpateSiswaVerification sqlUpateSiswaVerification = new SQLUpateSiswaVerification();

    @Given("^user click menu verifikasi data$")
    public void userClickMenuVerifikasiData() {
        commonPage.clickBtnTogle();
        verifikasiDataPage.clickBtnMenuVerifikasiData();
    }


    @Then("^user should be in verifikasi data Page$")
    public void userShouldBeInVerifikasiDataPage() {
        assertThat("Title page is wrong", verifikasiDataPage.getPageTitle(),
                containsString("Verifikasi Data"));
        assertThat("Title data is wrong", verifikasiDataPage.getTableTitle(),
                containsString("Verifikasi Data"));
    }

    @Then("^total student that is showed must be true$")
    public void totalStudentThatIsShowedMustBeTrue() {
        assertThat("Total data student is wrong", verifikasiDataPage.getTotalTableDataStudent(),
                equalTo(Integer.parseInt(verifikasiDataPage.getTotalStudent())));
    }

    @When("^user click first student in verifikasi data$")
    public void userClickFirstStudentInVerifikasiData() {
        verifikasiDataPage.clickFirstStudentCekBerkas();
    }

    @Then("^user in verifikasi data calon peserta page must be true$")
    public void userInVerifikasiDataCalonPesertaPageMustBeTrue() {
        assertThat("Student's name is wrong", verifikasiDataDiriPage.getStudentName(),
                equalToIgnoringCase(VerifikasiStudentData.getInstance().getName()));
        assertThat("Nomor UN is wrong", verifikasiDataDiriPage.getStudentNoUn(),
                equalToIgnoringCase(VerifikasiStudentData.getInstance().getNoUn()));
    }

    @And("^set first verifikasi student data$")
    public void setFirstVerifikasiStudentData() {
        VerifikasiStudentData.getInstance().setName(verifikasiDataPage.getFirstNameStudentData());
        VerifikasiStudentData.getInstance().setNoUn(verifikasiDataPage.getFirstNoUNStudentData());
    }

    @And("^move to student verifikasi data page$")
    public void moveToStudentVerifikasiDataPage() {
        commonPage.moveTabActive(1);
    }

    @When("^user verify student data's nilai$")
    public void userVerifyStudentDataSNilai() {
        verifikasiDataDiriPage.clickVerifikasiNilaiBenar();
    }

    @And("^user verify student data's domisili$")
    public void userVerifyStudentDataSDomisili() {
        verifikasiDataDiriPage.clickVerifikasiDomisili();
    }

    @And("^user verify student data's location$")
    public void userVerifyStudentDataSLocation() {
        verifikasiDataDiriPage.clickVerifikasiLatLong();
        verifikasiDataDiriPage.clickOkInAlert();
    }

    @And("^user verify student data's berkas$")
    public void userVerifyStudentDataSBerkas() {
        verifikasiDataDiriPage.clickVerifikasiBerkas();
    }

    @And("^user give final verification to student data$")
    public void userGiveFinalVerificationToStudentData() {
        verifikasiDataDiriPage.clickFinalVerificartion();
        verifikasiDataDiriPage.clickConfirmInFinalVerifikasi();
    }

    @Then("^student data must be verificated$")
    public void studentDataMustBeVerificated() {
        assertThat("Alert succes is not showed or wrong", verifikasiDataPage.getAlertSuccessVerification(),
                containsString("peserta berhasil diverifikasi"));
        assertThat("Student still in list of not verification student",
                verifikasiDataPage.isStudentInTableVerification(VerifikasiStudentData.getInstance().getNoUn()),
                equalTo(false));
    }

    @And("^set student personal data$")
    public void setStudentPersonalData() {
        VerifikasiStudentData.getInstance().setNoUn(verifikasiDataDiriPage.getStudentNoUn());
    }

    @And("^user click student in verifikasi data with nomor UN - nilai$")
    public void userClickStudentInVerifikasiDataWithNomorUNNilai() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnNilaiBenar());
    }

    @Then("^student data's nilai is verificated$")
    public void studentDataSNilaiIsVerificated() {
        assertThat("Alert success verification nilai is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Nilai"));
        assertThat("Student nilai data is not verificated",
                verifikasiDataDiriPage.isDataNilaiVerificationSuccess(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusNilaiIntoNormal(VerifikasiDataProperties.getInstance().getNoUnNilaiBenar());
    }

    @And("^user click student in verifikasi data with nomor UN - nilai wrong$")
    public void userClickStudentInVerifikasiDataWithNomorUNNilaiWrong() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnNilaiSalah());
    }

    @When("^user click wrong verify student data's nilai$")
    public void userClickWrongVerifyStudentDataSNilai() {
        verifikasiDataDiriPage.clickVerifikasiNilaiSalah();
    }

    @Then("^student data's nilai is wrong verificated$")
    public void studentDataSNilaiIsWrongVerificated() {
        assertThat("Alert success verification nilai is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Nilai"));
        assertThat("Student nilai data is not wrong verificated",
                verifikasiDataDiriPage.isDataNilaiVerificationWrong(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusNilaiIntoNormal(VerifikasiDataProperties.getInstance().getNoUnNilaiSalah());
    }

    @And("^user click student in verifikasi data with nomor UN - domisili$")
    public void userClickStudentInVerifikasiDataWithNomorUNDomisili() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnDomisiliBenar());
    }

    @Then("^student data's domisili is verificated$")
    public void studentDataSDomisiliIsVerificated() {
        assertThat("Alert success verification domisili is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Domisili"));
        assertThat("Student domisili data is not verificated",
                verifikasiDataDiriPage.isDataDomisiliVerificationSuccess(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusDomisiliIntoNormal(VerifikasiDataProperties.getInstance().getNoUnDomisiliBenar());
    }

    @And("^user click student in verifikasi data with nomor UN - domisili wrong$")
    public void userClickStudentInVerifikasiDataWithNomorUNDomisiliWrong() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnDomisiliSalah());
    }

    @When("^user click wrong verify student data's domisili$")
    public void userClickWrongVerifyStudentDataSDomisili() {
        verifikasiDataDiriPage.clickVerifikasiWrongDomisili();
    }

    @Then("^student data's domisili is wrong verificated$")
    public void studentDataSDomisiliIsWrongVerificated() {
        assertThat("Alert success verification domisili is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Domisili"));
        assertThat("Student domisili data is not wrong verificated",
                verifikasiDataDiriPage.isDataDomisiliVerificationWrong(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusDomisiliIntoNormal(VerifikasiDataProperties.getInstance().getNoUnDomisiliSalah());
    }

    @And("^user click student in verifikasi data with nomor UN - location$")
    public void userClickStudentInVerifikasiDataWithNomorUNLocation() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnLocationBenar());
    }

    @Then("^student data's location is verificated$")
    public void studentDataSLocationIsVerificated() {
        assertThat("Alert success verification location is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Lokasi Rumah"));
        assertThat("Student location data is not verificated",
                verifikasiDataDiriPage.isDataLocationVerificationSuccess(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusLonglatIntoNormal(VerifikasiDataProperties.getInstance().getNoUnLocationBenar());
    }

    @And("^user click wrong verify student data's location$")
    public void userClickWrongVerifyStudentDataSLocation() {
        verifikasiDataDiriPage.clickVerifikasiWrongLatlong();
    }

    @And("^user click student in verifikasi data with nomor UN - location wrong$")
    public void userClickStudentInVerifikasiDataWithNomorUNLocationWrong() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnLocationSalah());
    }

    @Then("^student data's location is wrong verificated$")
    public void studentDataSLocationIsWrongVerificated() {
        assertThat("Alert success verification location is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Lokasi Rumah"));
        assertThat("Student location data is not wrong verificated",
                verifikasiDataDiriPage.isDataLocationVerificationWrong(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusLonglatIntoNormal(VerifikasiDataProperties.getInstance().getNoUnLocationSalah());
    }

    @And("^user click student in verifikasi data with nomor UN - berkas$")
    public void userClickStudentInVerifikasiDataWithNomorUNBerkas() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnBerkasBenar());
    }

    @Then("^student data's berkas is verificated$")
    public void studentDataSBerkasIsVerificated() {
        assertThat("Alert success verification location is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Berkas"));
        assertThat("Student berkas data is not verificated",
                verifikasiDataDiriPage.isDataBerkasVerificationSuccess(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusBerkasIntoNormal(VerifikasiDataProperties.getInstance().getNoUnBerkasBenar());
    }

    @And("^user click student in verifikasi data with nomor UN - berkas wrong$")
    public void userClickStudentInVerifikasiDataWithNomorUNBerkasWrong() {
        verifikasiDataPage.clickCekBerkasWithNoUn(VerifikasiDataProperties.getInstance().getNoUnBerkasSalah());
    }

    @When("^user click wrong verify student data's berkas$")
    public void userClickWrongVerifyStudentDataSBerkas() {
        verifikasiDataDiriPage.clickVerifikasiWrongBerkas();
    }

    @Then("^student data's berkas is wrong verificated$")
    public void studentDataSBerkasIsWrongVerificated() {
        assertThat("Alert success verification location is wrong", verifikasiDataDiriPage.getAlertSuccess(),
                containsString("Berkas"));
        assertThat("Student berkas data is not verificated",
                verifikasiDataDiriPage.isDataBerkasVerificationWrong(),
                equalTo(true));
        sqlUpateSiswaVerification.UpdateSiswaStatusBerkasIntoNormal(VerifikasiDataProperties.getInstance().getNoUnBerkasSalah());
    }
}
