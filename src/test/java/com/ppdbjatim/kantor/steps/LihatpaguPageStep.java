package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.pages.CommonPage;
import com.ppdbjatim.kantor.pages.LihatpaguPage;
import com.ppdbjatim.kantor.properties.KelolaPaguProperties;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class LihatpaguPageStep extends ScenarioSteps {
    private LihatpaguPage lihatpaguPage;
    CommonPage commonPage;

    @Given("^user click menu lihat pagu$")
    public void userClickMenuLihatPagu() {
        commonPage.clickBtnTogle();
        lihatpaguPage.clickBtnLihatPagu();
    }

    @Then("^user should be in lihat pagu page$")
    public void userShouldBeInLihatPaguPage() {
        assertThat("Title Lihat Pagu is wrong", lihatpaguPage.getLihatPaguTitle().toLowerCase(),
                containsString("lihat pagu"));
    }

    @When("^user select one city in lihat pagu page$")
    public void userSelectOneCityInLihatPaguPage() {
        lihatpaguPage.clickSelectKotaKabupaten(KelolaPaguProperties.getInstance().getKotaKab());
    }

    @Then("^data pagu should be corresponding with the city$")
    public void dataPaguShouldBeCorrespondingWithTheCity() {
        assertThat("City in the title is wrong", lihatpaguPage.getLihatPaguTitle(),
                containsString(KelolaPaguProperties.getInstance().getKotaKab()));
        assertThat("City selected is wrong", lihatpaguPage.getSelectedKotaKabupaten(),
                containsString(KelolaPaguProperties.getInstance().getKotaKab()));
    }

}
