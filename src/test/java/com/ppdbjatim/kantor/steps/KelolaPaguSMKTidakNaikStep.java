package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.PaguData;
import com.ppdbjatim.kantor.pages.KelolaPaguSMKTidakNaikPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class KelolaPaguSMKTidakNaikStep extends ScenarioSteps {
    KelolaPaguSMKTidakNaikPage kelolaPaguSMKTidakNaikPage;

    @And("^user choose one school SMK Tidak Naik$")
    public void userChooseOneSchoolSMKTidakNaik() {
        kelolaPaguSMKTidakNaikPage.clickSelectFirstSchool();
    }

    @When("^user click edit button in first school SMK Tidak Naik$")
    public void userClickEditButtonInFirstSchoolSMKTidakNaik() {
        kelolaPaguSMKTidakNaikPage.clickFirstSchoolEditPaguTidakNaik();
    }

    @And("^user change pagu in SMK Tidak Naik$")
    public void userChangePaguInSMKTidakNaik() {
        kelolaPaguSMKTidakNaikPage.changeFirstPaguTidakNaik(kelolaPaguSMKTidakNaikPage.checkPaguTidakNaik());
    }

    @And("^set data pagu in SMK Tidak Naik$")
    public void setDataPaguInSMKTidakNaik() {
        PaguData.getInstance().setPaguTidakNaik(kelolaPaguSMKTidakNaikPage.getFirstInputPaguTidakNaik());
    }

    @And("^user click save edit pagu SMK Tidak Naik$")
    public void userClickSaveEditPaguSMKTidakNaik() {
        kelolaPaguSMKTidakNaikPage.clickFirstBtnSsvePaguTidakNaik();
    }

    @Then("^data pagu SMK tidak naik must be changed$")
    public void dataPaguSMKTidakNaikMustBeChanged() {
        assertThat("Data pagu tidak naik is not changed", kelolaPaguSMKTidakNaikPage.getFirstSchoolPaguTidakNaik(),
                equalTo(PaguData.getInstance().getPaguTidakNaik()));
    }
}
