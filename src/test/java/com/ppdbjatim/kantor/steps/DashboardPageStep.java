package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.pages.CommonPage;
import com.ppdbjatim.kantor.pages.DashboardPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DashboardPageStep extends ScenarioSteps {
    DashboardPage dashboardPage;
    CommonPage commonPage;

    @And("^account name in dashboard should be true Admin$")
    public void accountNameInDashboardShouldBeTrueAdmin() {
        commonPage.clickBtnTogle();
        assertThat("Account Name is wrong", dashboardPage.getAccountName(), equalToIgnoringCase("Administrator"));
    }

    @Then("^user should be logged in and in dashboard page$")
    public void userShouldBeLoggedInAndInDashboardPage() {
        assertThat("User not in dashboard", dashboardPage.getDashboardTitle().toLowerCase(), containsString("dashboard"));
    }

    @Given("^user click menu lihat pagu dashboard$")
    public void userClickMenuLihatPaguDashboard() {
        commonPage.clickBtnTogle();
        dashboardPage.clickBtnLihatPagu();
    }
}
