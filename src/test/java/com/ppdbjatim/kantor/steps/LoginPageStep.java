package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.pages.LoginPage;
import com.ppdbjatim.kantor.properties.LoginProperties;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class LoginPageStep extends ScenarioSteps {
    LoginPage loginPage;

    @Given("^user in kantor ppdbjatim login page$")
    public void userInKantorPpdbjatimLoginPage() {
        loginPage.open();
    }

    @When("^user insert username Admin$")
    public void userInsertUsernameAdmin() {
        loginPage.insertUsername(LoginProperties.getInstance().getUsernameAdmin());
    }

    @Then("^username should be appear Admin$")
    public void usernameShouldBeAppearAdmin() {
        assertThat("Input Username do not appear", loginPage.getUsernameAppear().isEmpty(), equalTo(false));
    }

    @And("^usernam should be same with the insertion Admin$")
    public void usernamShouldBeSameWithTheInsertionAdmin() {
        assertThat("Input Username is different", loginPage.getUsernameAppear(), equalToIgnoringCase(LoginProperties.getInstance().getUsernameAdmin()));
    }

    @When("^user insert password Admin$")
    public void userInsertPasswordAdmin() {
        loginPage.insertPassword(LoginProperties.getInstance().getPasswordAdmin());
    }

    @Then("^password should be appeared Admin$")
    public void passwordShouldBeAppearedAdmin() {
        assertThat("Password do not appear", loginPage.getPasswordAppear().isEmpty(), equalTo(false));
    }

    @And("^password should be same with the insertion Admin$")
    public void passwordShouldBeSameWithTheInsertionAdmin() {
        assertThat("Input password is different", loginPage.getPasswordAppear(), equalTo(LoginProperties.getInstance().getPasswordAdmin()));
    }

    @And("^user click login button$")
    public void userClickLoginButton() {
        loginPage.clickBtnLogin();
    }

    @And("^user insert username sekolah$")
    public void userInsertUsernameSekolah() {
        loginPage.insertUsername(LoginProperties.getInstance().getUsernameSekolah());
    }

    @And("^user insert password sekolah$")
    public void userInsertPasswordSekolah() {
        loginPage.insertPassword(LoginProperties.getInstance().getPasswordSekolah());
    }

    @And("^user insert username sekolah-2$")
    public void userInsertUsernameSekolah2() {
        loginPage.insertUsername(LoginProperties.getInstance().getUsernameSekolah2());
    }

    @And("^user insert password sekolah-2$")
    public void userInsertPasswordSekolah2() {
        loginPage.insertPassword(LoginProperties.getInstance().getPasswordSekolah2());
    }

    @And("^user insert username cabang dinas$")
    public void userInsertUsernameCabangDinas() {
        loginPage.insertUsername(LoginProperties.getInstance().getUsernameCabangDinas());
    }

    @And("^user insert password cabang dinas$")
    public void userInsertPasswordCabangDinas() {
        loginPage.insertPassword(LoginProperties.getInstance().getPasswordCabangDinas());
    }

    @And("^user insert username sekolah SMK$")
    public void userInsertUsernameSekolahSMK() {
        loginPage.insertUsername(LoginProperties.getInstance().getUsernameSekolahSMK());
    }

    @And("^user insert password sekolah SMK$")
    public void userInsertPasswordSekolahSMK() {
        loginPage.insertPassword(LoginProperties.getInstance().getPasswordSekolahSMK());
    }
}
