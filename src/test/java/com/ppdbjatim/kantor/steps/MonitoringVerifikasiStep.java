package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.pages.CommonPage;
import com.ppdbjatim.kantor.pages.MonitoringVerifikasiPage;
import com.ppdbjatim.kantor.pages.VerifikasiDataDiriPage;
import com.ppdbjatim.kantor.properties.MonitoringVerifikasProperties;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import javax.smartcardio.CommandAPDU;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MonitoringVerifikasiStep extends ScenarioSteps {
    MonitoringVerifikasiPage monitoringVerifikasiPage;
    VerifikasiDataDiriPage verifikasiDataDiriPage;
    CommonPage commonPage;

    @Given("^user click menu monitoring data verification$")
    public void userClickMenuMonitoringDataVerification() {
        commonPage.clickBtnTogle();
        monitoringVerifikasiPage.clickMenuVerification();
    }

    @Then("^user should be in monitoring data verification Page$")
    public void userShouldBeInMonitoringDataVerificationPage() {
        assertThat("Title page is wrong", monitoringVerifikasiPage.getTitlePage(),
                equalToIgnoringCase("Monitoring Data Input PIN"));
    }

    @When("^user insert no Un student wait for verification in searching$")
    public void userInsertNoUnStudentWaitForVerificationInSearching() {
        monitoringVerifikasiPage.inputNomorUn(MonitoringVerifikasProperties.getInstance().getNoUnBelumVerifikasi());
    }

    @Then("^data PIN should not exist$")
    public void dataPINShouldNotExist() {
        assertThat("PIN is exist", monitoringVerifikasiPage.getPINDataTable(),
                equalTo("-"));
    }

    @And("^data status is wait for verification$")
    public void dataStatusIsWaitForVerification() {
        assertThat("Status is not wait verification", monitoringVerifikasiPage.getStatusDataTable(),
                containsString("Menunggu"));
    }

    @When("^user insert no Un in searching$")
    public void userInsertNoUnInSearching() {
        monitoringVerifikasiPage.inputNomorUn(MonitoringVerifikasProperties.getInstance().getNoUnBelumVerifikasi());
    }

    @And("^user click button search$")
    public void userClickButtonSearch() {
        monitoringVerifikasiPage.clickBtnSearch();
    }

    @Then("^data result no Un should be same with input$")
    public void dataResultNoUnShouldBeSameWithInput() {
        assertThat("Nomor Un in data table is wrong", monitoringVerifikasiPage.getNoUnDataTable(),
                equalTo(MonitoringVerifikasProperties.getInstance().getNoUnBelumVerifikasi()));
    }

    @When("^user click button cek berkas in student data$")
    public void userClickButtonCekBerkasInStudentData() {
        monitoringVerifikasiPage.clickBtnCekBerkas();
    }

    @Then("^button verification student is still exist$")
    public void buttonVerificationStudentIsStillExist() {
        assertThat("Button Verification is not exist", verifikasiDataDiriPage.isButtonVerificationStudentExist(),
                equalTo(true));
    }

    @When("^user insert no Un student finish verification$")
    public void userInsertNoUnStudentFinishVerification() {
        monitoringVerifikasiPage.inputNomorUn(MonitoringVerifikasProperties.getInstance().getNoUnSudahVerifikasi());
    }

    @Then("^data PIN shold be exist$")
    public void dataPINSholdBeExist() {
        assertThat("PIN is not exist", monitoringVerifikasiPage.getPINDataTable(),
                not(equalTo("-")));
    }

    @And("^data status is finish verification$")
    public void dataStatusIsFinishVerification() {
        assertThat("Status is not wait verification", monitoringVerifikasiPage.getStatusDataTable(),
                containsString("Selesai"));
    }

    @Then("^button verification student is not exist$")
    public void buttonVerificationStudentIsNotExist() {
        assertThat("Button Verification is exist", verifikasiDataDiriPage.isButtonVerificationStudentExist(),
                equalTo(false));
    }

    @And("^page show status student is already verificated$")
    public void pageShowStatusStudentIsAlreadyVerificated() {
        assertThat("Status student is verificated is not showed", verifikasiDataDiriPage.isStatusSuccessVerificationStudentExist(),
                equalTo(true));
    }

    @When("^user insert no Un student rejected verification$")
    public void userInsertNoUnStudentRejectedVerification() {
        monitoringVerifikasiPage.inputNomorUn(MonitoringVerifikasProperties.getInstance().getNoUnTertolak());
    }

    @And("^data status is rejected verification$")
    public void dataStatusIsRejectedVerification() {
        assertThat("Staus is not rejected verification", monitoringVerifikasiPage.getStatusDataTable(),
                containsString("Revisi"));
    }

    @And("^page show rejected message$")
    public void pageShowRejectedMessage() {
        assertThat("Page is not showing rejected message", verifikasiDataDiriPage.isRejectedMessageShowed(),
                equalTo(true));
    }
}
