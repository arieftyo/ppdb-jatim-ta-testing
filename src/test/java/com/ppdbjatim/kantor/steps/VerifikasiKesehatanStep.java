package com.ppdbjatim.kantor.steps;

import com.ppdbjatim.kantor.data.HealthVerificationData;
import com.ppdbjatim.kantor.pages.CommonPage;
import com.ppdbjatim.kantor.pages.VerifikasiDataKesehatanPage;
import com.ppdbjatim.kantor.pages.VerifikasiKesehatanPage;
import com.ppdbjatim.kantor.properties.VerifikasiKesehatanProperties;
import com.ppdbjatim.kantor.sql.SQLUpdateSiswaHealthVerification;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class VerifikasiKesehatanStep extends ScenarioSteps {
    VerifikasiKesehatanPage verifikasiKesehatanPage;
    CommonPage commonPage;
    VerifikasiDataKesehatanPage verifikasiDataKesehatanPage;

    SQLUpdateSiswaHealthVerification sqlUpdateSiswaHealthVerification = new SQLUpdateSiswaHealthVerification();

    @Then("^user in verifikasi kesehatan page$")
    public void userInVerifikasiKesehatanPage() {
        assertThat("User not in verifikasi kesehatan page", verifikasiKesehatanPage.getPageTitle(),
                containsString("Tes Kesehatan"));
    }

    @Then("^total student in verifikasi kesehatan is true$")
    public void totalStudentInVerifikasiKesehatanIsTrue() {
        assertThat("Total student is wrong", verifikasiKesehatanPage.getTotalStudentShowed(),
                equalTo(verifikasiKesehatanPage.getTotalStudentInTable()));
    }

    @And("^user click menu verifikasi kesehatan$")
    public void userClickMenuVerifikasiKesehatan() {
        commonPage.clickBtnTogle();
        verifikasiKesehatanPage.clickBtnMenuVerifikasiKesehatan();
    }

    @Given("^user set data student in health verification$")
    public void userSetDataStudentInHealthVerification() {
        HealthVerificationData.getInstance().setName(verifikasiKesehatanPage.getNameWithNoUn(VerifikasiKesehatanProperties
                .getInstance().getNoUnKesehatanNotVerificated()));
        HealthVerificationData.getInstance().setNoUn(VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanNotVerificated());
    }

    @And("^move to student health verification data page$")
    public void moveToStudentHealthVerificationDataPage() {
        commonPage.moveTabActive(1);
    }

    @Then("^selected student data in health verification student must be true$")
    public void selectedStudentDataInHealthVerificationStudentMustBeTrue() {
        assertThat("Student data noUn is wrong", verifikasiDataKesehatanPage.getNoUnStudent(),
                equalTo(HealthVerificationData.getInstance().getNoUn()));
        assertThat("Student data nama is wrong", verifikasiDataKesehatanPage.getNamaStudent(),
                equalTo(HealthVerificationData.getInstance().getName()));
    }

    @When("^user click cek berkas in student health data- not verificated$")
    public void userClickCekBerkasInStudentHealthDataNotVerificated() {
        commonPage.scrollWindow();
        verifikasiKesehatanPage.clickBtnCekBerkasByNoUn(VerifikasiKesehatanProperties.getInstance()
                .getNoUnKesehatanNotVerificated());
    }

    @Then("^information status student in health verification is wait for verification$")
    public void informationStatusStudentInHealthVerificationIsWaitForVerification() {
        assertThat("Status student is not waiting for verification",
                verifikasiKesehatanPage.getStatusInformationByUn(VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanNotVerificated()),
                containsString("Menunggu"));
    }

    @Then("^button health verification is exist$")
    public void buttonHealthVerificationIsExist() {
        assertThat("Button Health verification is not exist", verifikasiDataKesehatanPage.isButtonHealthVerificationExist(),
                equalTo(true));
    }

    @And("^button file wrong health verification is exist$")
    public void buttonFileWrongHealthVerificationIsExist() {
        assertThat("Button Wrong file health verification is not exist", verifikasiDataKesehatanPage.isButtonWrongFileHealthExist(),
                equalTo(true));
    }

    @Then("^information status student in health verification is success for verification$")
    public void informationStatusStudentInHealthVerificationIsSuccessForVerification() {
        assertThat("Information status is not success verificated",
                verifikasiKesehatanPage.getStatusInformationByUn(VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanVerificated()),
                containsString("Selesai"));
    }

    @Given("^user click cek berkas in student health data- verificated$")
    public void userClickCekBerkasInStudentHealthDataVerificated() {
        verifikasiKesehatanPage.clickBtnCekBerkasByNoUn(VerifikasiKesehatanProperties.getInstance()
                .getNoUnKesehatanVerificated());
    }

    @Then("^button health verification is not exist$")
    public void buttonHealthVerificationIsNotExist() {
        assertThat("Button Health verification is not exist", verifikasiDataKesehatanPage.isButtonHealthVerificationExist(),
                equalTo(false));
    }

    @And("^button file wrong health verification is not exist$")
    public void buttonFileWrongHealthVerificationIsNotExist() {
        assertThat("Button Wrong file health verification is not exist", verifikasiDataKesehatanPage.isButtonWrongFileHealthExist(),
                equalTo(false));
    }


    @And("^status verification done is showed$")
    public void statusVerificationDoneIsShowed() {
        assertThat("Status verification done is not showed", verifikasiDataKesehatanPage.isButtonStatusVerificationDoneIsExist(),
                equalTo(true));
    }

    @Given("^user click cek berkas in student health data- rejected$")
    public void userClickCekBerkasInStudentHealthDataRejected() {
        verifikasiKesehatanPage.clickBtnCekBerkasByNoUn(VerifikasiKesehatanProperties.getInstance()
                .getNoUnKesehatanRejected());
    }

    @And("^button status wait for revision is showed disabled$")
    public void buttonStatusWaitForRevisionIsShowedDisabled() {
        assertThat("Status wait for revision is not showed disabled",
                verifikasiDataKesehatanPage.isStatusWaitRevisionShowedDisabled(),
                equalTo(true));
    }

    @Then("^information status student in health verification is wait for revision$")
    public void informationStatusStudentInHealthVerificationIsWaitForRevision() {
        assertThat("Information status is not success verificated",
                verifikasiKesehatanPage.getStatusInformationByUn(VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanRejected()),
                containsString("Menunggu Revisi"));
    }

    @When("^change status info colour blind into positive colour blind$")
    public void changeStatusInfoColourBlindIntoPositiveColourBlind() {
        verifikasiDataKesehatanPage.clickNotBlindColour();
        verifikasiDataKesehatanPage.clickBlindColour();
    }

    @And("^change status info body tall into (.*)$")
    public void changeStatusInfoBodyTallInto(String bodyTall) {
        verifikasiDataKesehatanPage.insertBodyTall(bodyTall);
    }

    @And("^verify accept student health data$")
    public void verifyAcceptStudentHealthData() {
        verifikasiDataKesehatanPage.clickAcceptVerifyHealth();
        verifikasiDataKesehatanPage.clickConfirmAcceptVerification();
    }

    @Then("^status student is success health verification$")
    public void statusStudentIsSuccessHealthVerification() {
        assertThat("Information status is not success verificated",
                verifikasiKesehatanPage.getStatusInformationByUn(VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanNotVerificated()),
                containsString("Selesai"));
    }

    @Then("^status info colour blind is changed same with positive$")
    public void statusInfoColourBlindIsChangedSameWithPositive() {
        assertThat("Status colour blind is not changed into positive", verifikasiDataKesehatanPage.getStatusInfoBlindColour(),
                equalTo("Buta Warna"));
    }

    @And("^status body tall is changed same with (.*)$")
    public void statusBodyTallIsChangedSameWith(String bodyTall) {
        assertThat("Staus body tall is not changed into inputed data", verifikasiDataKesehatanPage.getStatusInfoBodyTall(),
                equalTo(bodyTall));
    }

    @And("^move to student health verification data page (\\d+)$")
    public void moveToStudentHealthVerificationDataPage(int page) {
        commonPage.moveTabActive(page);
    }


    @And("^make student status data health verification into normal - not verificated$")
    public void makeStudentStatusDataHealthVerificationIntoNormalNotVerificated() {
        sqlUpdateSiswaHealthVerification.UpdateSiswaStatusHealthVerificationNormal(
                VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanNotVerificated());
    }

    @Given("^user click cek berkas in student health data- not verificated rejected$")
    public void userClickCekBerkasInStudentHealthDataNotVerificatedRejected() {
        verifikasiKesehatanPage.clickBtnCekBerkasByNoUn(VerifikasiKesehatanProperties.getInstance()
                .getNoUnKesehatanNotVerificated2());
    }

    @When("^reject student health verification$")
    public void rejectStudentHealthVerification() {
        verifikasiDataKesehatanPage.clickBtnFileWrong();
        HealthVerificationData.getInstance().setMessageWrongFile(verifikasiDataKesehatanPage.getMessageFileWrong());
        verifikasiDataKesehatanPage.clickMessageFileWrong();
        verifikasiDataKesehatanPage.clickBtnSaveFileWrong();
    }

    @And("^message wrong file is same with input$")
    public void messageWrongFileIsSameWithInput() {
        assertThat("Message wrong file is wrong", verifikasiDataKesehatanPage.getMessageFileWrongShowed(),
                containsString(HealthVerificationData.getInstance().getMessageWrongFile()));
    }

    @And("^make student status data health verification into normal - not verificated rejected$")
    public void makeStudentStatusDataHealthVerificationIntoNormalNotVerificatedRejected() {
        sqlUpdateSiswaHealthVerification.UpdateSiswaStatusHealthVerificationNormal(
                VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanNotVerificated2());
    }

    @Then("^information status student in health verification is wait for revision - not verificated rejected$")
    public void informationStatusStudentInHealthVerificationIsWaitForRevisionNotVerificatedRejected() {
        assertThat("Information status is not success verificated",
                verifikasiKesehatanPage.getStatusInformationByUn(VerifikasiKesehatanProperties.getInstance().getNoUnKesehatanNotVerificated2()),
                containsString("Menunggu Revisi"));
    }
}
