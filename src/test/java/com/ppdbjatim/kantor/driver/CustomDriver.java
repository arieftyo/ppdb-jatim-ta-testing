package com.ppdbjatim.kantor.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Arrays;

public class CustomDriver implements DriverSource {
    @Override
    public WebDriver newDriver() {
        EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();
        String browser = EnvironmentSpecificConfiguration.from(env).getProperty("browser.data");


        if (browser.equalsIgnoreCase("chrome"))
        {
            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--ignore-ssl-errors=yes");
            options.addArguments("--ignore-certificate-errors");
            options.addArguments("start-maximized");
            options.addArguments("enable-automation");
            options.addArguments("--headless");
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-infobars");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--disable-browser-side-navigation");
            options.addArguments("--disable-gpu");
            ChromeDriver driver = new ChromeDriver(options);
            driver.manage().window().maximize();
            return driver;
        }
        else if(browser.equalsIgnoreCase("firefox"))
        {
            ProfilesIni profile = new ProfilesIni();
            FirefoxProfile ffProfile = profile.getProfile("default");
            ffProfile.setAcceptUntrustedCertificates(true);
            ffProfile.setAssumeUntrustedCertificateIssuer(true);
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setCapability(FirefoxDriver.PROFILE, ffProfile);
            FirefoxDriver driver = new FirefoxDriver(firefoxOptions);
            driver.manage().window().maximize();
            return driver;
        }
        else
        {
            return null;
        }
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
