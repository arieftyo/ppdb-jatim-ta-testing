package com.ppdbjatim.kantor.properties;

import lombok.Getter;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

@Getter
public class KelolaPaguProperties {
    private static KelolaPaguProperties instance= null;
    public static KelolaPaguProperties getInstance()
    {
        if (instance == null){
            instance = new KelolaPaguProperties();
        }
        return instance;
    }

    EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();
    String kotaKab = EnvironmentSpecificConfiguration.from(env).getProperty("kelolapagu.kota");
    String exceededRombel = EnvironmentSpecificConfiguration.from(env).getProperty("kelolapagu.exceeded.rombel");
    String bidangKeahlianSMK = EnvironmentSpecificConfiguration.from(env).getProperty("kelolapagu.bidangkeahlian.smk");
    String paketKeahlianSMK = EnvironmentSpecificConfiguration.from(env).getProperty("kelolapagu.paketkeahlian.smk");
    String jurusanRombelSMK = EnvironmentSpecificConfiguration.from(env).getProperty("kelolapagu.jurusanrombel.smk");
    String jurusanSiswaSMK = EnvironmentSpecificConfiguration.from(env).getProperty("kelolapagu.jurusansiswa.smk");
    String deleteJurusanSMK = EnvironmentSpecificConfiguration.from(env).getProperty("kelolapagu.bidangkeahlian.hapus");
}
