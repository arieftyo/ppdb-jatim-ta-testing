package com.ppdbjatim.kantor.properties;

import lombok.Getter;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

@Getter
public class MonitoringVerifikasProperties {
    private static MonitoringVerifikasProperties instance= null;
    public static MonitoringVerifikasProperties getInstance()
    {
        if (instance == null){
            instance = new MonitoringVerifikasProperties();
        }
        return instance;
    }

    EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();
    String noUnBelumVerifikasi = EnvironmentSpecificConfiguration.from(env).getProperty("monitoring.nomorUn.notVerificated");
    String noUnSudahVerifikasi = EnvironmentSpecificConfiguration.from(env).getProperty("monitoring.nomorUn.verificated");
    String noUnTertolak = EnvironmentSpecificConfiguration.from(env).getProperty("monitoring.nomorUn.rejected");


}
