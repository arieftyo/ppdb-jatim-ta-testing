package com.ppdbjatim.kantor.properties;

import lombok.Getter;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

@Getter
public class VerifikasiKesehatanProperties {
    private static VerifikasiKesehatanProperties instance= null;
    public static VerifikasiKesehatanProperties getInstance()
    {
        if (instance == null){
            instance = new VerifikasiKesehatanProperties();
        }
        return instance;
    }

    EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();
    String noUnKesehatanNotVerificated = EnvironmentSpecificConfiguration.from(env).getProperty("kesehatan.nomorUn.notVerificated");
    String noUnKesehatanVerificated = EnvironmentSpecificConfiguration.from(env).getProperty("kesehatan.nomorUn.verificated");
    String noUnKesehatanRejected = EnvironmentSpecificConfiguration.from(env).getProperty("kesehatan.nomorUn.rejected");
    String noUnKesehatanNotVerificated2 = EnvironmentSpecificConfiguration.from(env).getProperty("kesehatan.nomorUn.notVerificated2");


}
