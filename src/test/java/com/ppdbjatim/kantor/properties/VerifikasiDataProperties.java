package com.ppdbjatim.kantor.properties;

import lombok.Getter;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

@Getter
public class VerifikasiDataProperties {
    private static VerifikasiDataProperties instance= null;
    public static VerifikasiDataProperties getInstance()
    {
        if (instance == null){
            instance = new VerifikasiDataProperties();
        }
        return instance;
    }

    EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();
    String noUnNilaiBenar = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.nilaiTrue");
    String noUnNilaiSalah = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.nilaiWrong");
    String noUnDomisiliBenar = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.domisiliTrue");
    String noUnDomisiliSalah = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.domisiliWrong");
    String noUnLocationBenar = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.locationTrue");
    String noUnLocationSalah = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.locationWrong");
    String noUnBerkasBenar = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.berkasTrue");
    String noUnBerkasSalah = EnvironmentSpecificConfiguration.from(env).getProperty("verifikasi.nomorUn.berkasWrong");





}
