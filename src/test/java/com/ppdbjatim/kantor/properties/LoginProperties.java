package com.ppdbjatim.kantor.properties;

import lombok.Getter;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

@Getter
public class LoginProperties {
    private static LoginProperties instance= null;
    public static LoginProperties getInstance()
    {
        if (instance == null){
            instance = new LoginProperties();
        }
        return instance;
    }

    EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();

    String usernameAdmin = EnvironmentSpecificConfiguration.from(env).getProperty("login.username.admin");
    String usernameSekolah = EnvironmentSpecificConfiguration.from(env).getProperty("login.username.sekolah");
    String passwordAdmin = EnvironmentSpecificConfiguration.from(env).getProperty("login.password.admin");
    String passwordSekolah = EnvironmentSpecificConfiguration.from(env).getProperty("login.password.sekolah");
    String usernameSekolah2 = EnvironmentSpecificConfiguration.from(env).getProperty("login.username.sekolah2");
    String passwordSekolah2 = EnvironmentSpecificConfiguration.from(env).getProperty("login.password.sekolah2");
    String usernameCabangDinas = EnvironmentSpecificConfiguration.from(env).getProperty("login.username.cabangdinas");
    String passwordCabangDinas = EnvironmentSpecificConfiguration.from(env).getProperty("login.password.cabangdinas");
    String passwordSekolahSMK = EnvironmentSpecificConfiguration.from(env).getProperty("login.password.sekolahsmk");
    String usernameSekolahSMK = EnvironmentSpecificConfiguration.from(env).getProperty("login.username.sekolahsmk");


}
