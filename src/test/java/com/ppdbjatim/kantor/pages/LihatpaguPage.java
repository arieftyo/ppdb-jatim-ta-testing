package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class LihatpaguPage extends PageObject {
    @FindBy(xpath = "//li//a[contains(@href, 'lihat_pagu')]")
    WebElementFacade btnLihatPagu;

    @FindBy(xpath = "//header[@class='page-header']//h2")
    WebElementFacade lihatPaguTitle;

    @FindBy(xpath = "//select[@name='kode_kota_kabupaten']")
    WebElementFacade selectKotaKabupatenPagu;

    public void clickBtnLihatPagu()
    {
        btnLihatPagu.click();
    }

    public String getLihatPaguTitle()
    {
        return lihatPaguTitle.getText();
    }

    public void clickSelectKotaKabupaten(String namaKota)
    {
        Select kotaKabupaten = new Select(selectKotaKabupatenPagu);
        kotaKabupaten.selectByVisibleText(namaKota);
    }

    public String getSelectedKotaKabupaten()
    {
        Select kotaKabupaten = new Select(selectKotaKabupatenPagu);
        return kotaKabupaten.getFirstSelectedOption().getText();
    }
}
