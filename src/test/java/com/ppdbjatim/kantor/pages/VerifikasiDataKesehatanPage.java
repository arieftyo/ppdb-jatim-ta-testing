package com.ppdbjatim.kantor.pages;

import jxl.StringFormulaCell;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class VerifikasiDataKesehatanPage extends PageObject {
    @FindBy(xpath = "//div[@class='card-body']//div[@class='form-group row']//div[@class='col-md-9']")
    List<WebElementFacade> dataDiri;

    @FindBy(xpath = "//div[@class='card-body']//div[@class='row']//div[@class='checkbox-group required butawarna']//input[@value='Buta Warna']")
    WebElementFacade inputCheckBoxBlindColour;

    @FindBy(xpath = "//div[@class='card-body']//div[@class='row']//div[@class='checkbox-group required butawarna']//input[@value='Tidak Buta Warna']")
    WebElementFacade inputCheckBoxNotBlindColour;

    @FindBy(xpath = "(//form[@id='form-validasi']//input[@type='text'])[1]")
    WebElementFacade statusBlindColour;

    @FindBy(xpath = "(//form[@id='form-validasi']//input[@type='text'])[2]")
    WebElementFacade statusBodyTall;

    @FindBy(xpath = "//form[@id='form-validasi']//input[@name='smp_tes_tinggi_badan']")
    WebElementFacade inputBodyTall;

    @FindBy(xpath = "//div[@class='card-footer text-right']//button[@id='btn-confirm']")
    List<WebElementFacade> btnVerifyHealthData;

    @FindBy(xpath = "//div[@id='verifModal']//div[@class='modal-content']//button[@id='btn-confirm']")
    WebElementFacade btnConfirmVerifyHealthData;

    @FindBy(xpath = "//div[@class='card-footer text-right']//button[@class='btn btn-danger']")
    List<WebElementFacade> btnFileWrong;

    @FindBy(xpath = "//div[@id='berkasModal']//form//input[@value='File Kesehatan buram / tidak terbaca']")
    WebElementFacade messageWrongFile;

    @FindBy(xpath = "//div[@id='berkasModal']//form//div[@class='modal-footer']//button[@class='btn btn-primary']")
    WebElementFacade btnSaveWrongFile;

    @FindBy(xpath = "//form[@id='form-validasi']//div[@class='card-footer text-right']//button[@class='btn btn-success']")
    List<WebElementFacade> btnStatusVerificationDone;

    @FindBy(xpath = "//form[@id='form-validasi']//div[@class='alert alert-danger']")
    WebElementFacade messageWrongFileShowed;

    public String getNoUnStudent()
    {
        String noUn = dataDiri.get(0).getText();
        noUn= noUn.replace(": ", "");
        return noUn;
    }

    public String getNamaStudent()
    {
        String nama = dataDiri.get(1).getText();
        nama = nama.replace(": ","");
        return nama;
    }

    public boolean isButtonHealthVerificationExist()
    {
        return btnVerifyHealthData.size() != 0;
    }

    public boolean isButtonWrongFileHealthExist()
    {
        return btnFileWrong.size() != 0;
    }

    public boolean isButtonStatusVerificationDoneIsExist()
    {
        return btnStatusVerificationDone.size() != 0;
    }

    public boolean isStatusWaitRevisionShowedDisabled()
    {
        return btnFileWrong.get(0).isDisabled();
    }

    public void clickBlindColour()
    {
        inputCheckBoxBlindColour.click();
    }

    public void clickNotBlindColour()
    {
        inputCheckBoxNotBlindColour.click();
    }

    public void insertBodyTall(String bodyTall)
    {
        inputBodyTall.clear();
        inputBodyTall.sendKeys(bodyTall);
    }

    public void clickAcceptVerifyHealth()
    {
        btnVerifyHealthData.get(0).click();
    }

    public void clickConfirmAcceptVerification()
    {
        btnConfirmVerifyHealthData.click();
    }

    public String getStatusInfoBlindColour()
    {
        return statusBlindColour.getAttribute("placeholder");
    }

    public String getStatusInfoBodyTall()
    {
        return statusBodyTall.getAttribute("value");
    }

    public void clickBtnFileWrong()
    {
        btnFileWrong.get(0).click();
    }

    public void clickMessageFileWrong()
    {
        messageWrongFile.click();
    }

    public void clickBtnSaveFileWrong()
    {
        btnSaveWrongFile.click();
    }

    public String getMessageFileWrong()
    {
        return messageWrongFile.getText();
    }

    public String getMessageFileWrongShowed()
    {
        return messageWrongFileShowed.getText();
    }
}
