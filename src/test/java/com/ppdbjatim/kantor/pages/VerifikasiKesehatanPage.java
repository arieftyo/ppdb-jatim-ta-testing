package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class VerifikasiKesehatanPage extends PageObject {
    @FindBy(xpath = "(//nav[@class='side-navbar shrinked']//ul//li//a[contains(@href,'teskesehatan')])[1]")
    WebElementFacade btnMenuVerifikasiKesehatan;

    @FindBy(xpath = "//div[@class='content-inner']//header//div[@class='container-fluid']//h2")
    WebElementFacade titlePageKesehatanVerification;

    @FindBy(xpath = "//section[@class='forms']//div[@class='row']//div[@class='title']//div[@class='number']//strong")
    WebElementFacade totalStudent;

    @FindBy(xpath = "//form[@id='validasi-bidikmisi']//div[@id='table-validasi-offline_info']")
    WebElementFacade infoTotalStudentInTable;

    @FindBy(xpath = "//div[@class='table-responsive']//table[@id='table-validasi-offline']//tbody//tr//td[2]")
    List<WebElementFacade> dataNoUnTable;

    @FindBy(xpath = "//div[@class='table-responsive']//table[@id='table-validasi-offline']//tbody//tr//td[5]//a")
    List<WebElementFacade> btnCekBerkasTable;

    @FindBy(xpath = "//a[@class='paginate_button next disabled']")
    List<WebElementFacade> btnNextPageDisabled;

    @FindBy(xpath = "//a[@id='table-validasi-offline_next']")
    WebElementFacade btnNextPage;

    public void clickBtnMenuVerifikasiKesehatan()
    {
       btnMenuVerifikasiKesehatan.click();
    }

    public String getPageTitle()
    {
        return titlePageKesehatanVerification.getText();
    }

    public String getTotalStudentShowed()
    {
        return totalStudent.getText();
    }

    public String getTotalStudentInTable()
    {
        String text = infoTotalStudentInTable.getText();
        text = text.replaceAll("\\s+","");
        text = text.replaceAll(",", "");
        String totalStudent = text.substring(text.indexOf("f")+1, text.indexOf("e"));
        return totalStudent;
    }

    public String getNameWithNoUn(String noUn)
    {
        WebElementFacade name = findBy("//td[contains(text(),'"+ noUn +"')]/following-sibling::td[1]");
        return name.getText();
    }

    public void clickBtnCekBerkasByNoUn(String noUn)
    {
        int i;
        int state = 0;
        Actions actions = new Actions(getDriver());
//        while (true)
//        {
            i = 0;
            for (WebElementFacade webElementFacade : dataNoUnTable) {
                if (webElementFacade.getText().equalsIgnoreCase(noUn)) {
                    actions.keyDown(Keys.LEFT_CONTROL)
                            .click( btnCekBerkasTable.get(i))
                            .keyUp(Keys.LEFT_CONTROL)
                            .build()
                            .perform();
                    break;
                }
                i++;
//            }
//            break;
//
//            if (btnNextPageDisabled.size() != 0)
//            {
//                break;
//            }
//            btnNextPage.click();
        }
    }

    public String getStatusInformationByUn(String noUn)
    {
        WebElementFacade name = findBy("//td[contains(text(),'"+ noUn +"')]/following-sibling::td[2]");
        return name.getText();
    }
}
