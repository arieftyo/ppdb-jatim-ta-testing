package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class MonitoringVerifikasiPage extends PageObject {
    @FindBy(xpath = "(//ul[@class='list-styled']//a[contains(@href,'verifikasidata/monitoring')])[1]")
    WebElementFacade btnMenuMonitoringVerification;

    @FindBy(xpath = "//div[@class='content-inner']//header//div[@class='container-fluid']//h2")
    WebElementFacade titlePageMonitoringVerification;

    @FindBy(xpath = "//input[@name='smp_uasbn']")
    WebElementFacade inputUn;

    @FindBy(xpath = "//form//button[@class='btn btn-info']")
    WebElementFacade btnSearch;

    @FindBy(xpath = "//table[@id='table-validasi-offline']//tbody//tr//td")
    List<WebElementFacade> dataInTable;

    @FindBy(xpath = "(//table[@id='table-validasi-offline']//tbody//tr//td//span)[3]")
    WebElementFacade dataStatus;

    @FindBy(xpath = "//table[@id='table-validasi-offline']//tbody//tr//td//a[@class='btn btn-sm btn-success']")
    WebElementFacade btnCekBerkas;

    public void clickMenuVerification()
    {
        btnMenuMonitoringVerification.click();
    }

    public String getTitlePage()
    {
        return titlePageMonitoringVerification.getText();
    }

    public void inputNomorUn(String noUn)
    {
        inputUn.sendKeys(noUn);
    }

    public void clickBtnSearch()
    {
        btnSearch.click();
    }

    public String getNoUnDataTable()
    {
        String nomorUn = dataInTable.get(0).getText();
        nomorUn= nomorUn.substring(0, nomorUn.indexOf(" "));
        return nomorUn;
    }

    public String getPINDataTable()
    {
        return dataInTable.get(2).getText();
    }

    public String getStatusDataTable()
    {
        return dataStatus.getText();
    }

    public void clickBtnCekBerkas()
    {
        btnCekBerkas.click();
    }

}
