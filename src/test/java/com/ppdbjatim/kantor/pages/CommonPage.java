package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;

public class CommonPage extends PageObject {

    @FindBy(xpath = "//nav[@class='navbar']//div[@class='navbar-header']//a[@id='toggle-btn']")
    WebElementFacade btnTogle;

    public void moveTabActive(int index)
    {
        ArrayList<String> tabs2 = new ArrayList<String> (getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs2.get(index));
    }

    public void scrollWindow()
    {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollBy(0,500)");
    }

    public void clickBtnTogle()
    {
        btnTogle.click();
    }
}
