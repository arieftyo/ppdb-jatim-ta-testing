package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class DaftarSelesaiVerifikasiPage extends PageObject {
    @FindBy(xpath = "(//a[contains(@href,'verifikasidata')])[2]")
    WebElementFacade btnMenuDaftarSelesaiVerifikasi;

    @FindBy(xpath = "//a[@id='table-validasi-offline_next']")
    WebElementFacade btnNextPage;

    @FindBy(xpath = "//a[@class='paginate_button next disabled']")
    List<WebElementFacade> btnNextPageDisabled;

    @FindBy(xpath = "//div[@class='table-responsive']//table[@id='table-validasi-offline']//tbody//tr//td[1]")
    List<WebElementFacade> dataNoUnTable;

    @FindBy(xpath = "//div[@id='table-validasi-offline_paginate']//span//a[@class='paginate_button ']")
    List<WebElementFacade> btnPage;

    @FindBy(xpath = "//section[@class='dashboard-counts no-padding-bottom']//div[@class='number']//strong")
    WebElementFacade totalStudents;

    @FindBy(xpath = "//div[@id='table-validasi-offline_paginate']//span//a[@class='paginate_button current']")
    WebElementFacade btnPageClicked;

    public void clickMenuDaftarSelesaiVerifikasi()
    {
        btnMenuDaftarSelesaiVerifikasi.click();
    }

    public boolean isStudentInTableSuccessVerification(String noUn)
    {
        while (true)
        {
            for (WebElementFacade nomorUnSiswa : dataNoUnTable) {
                if (nomorUnSiswa.getText().equalsIgnoreCase(noUn)) {
                    return true;
                }
                System.out.println(nomorUnSiswa.getText());
            }
            if (btnNextPageDisabled.size() != 0)
            {
                break;
            }
            btnNextPage.click();
        }
        return false;
    }

    public int getTotalSiswaInTabel()
    {
        int jumlah = 0;
        int totalPage = Integer.parseInt(btnPageClicked.getText());
        int totalSiswaLastPage = dataNoUnTable.size();

        jumlah = ((totalPage - 1) * 10 ) + totalSiswaLastPage;

        return jumlah;
    }

    public void clickBtnLastPagination()
    {
        btnPage.get(btnPage.size() - 1).click();
    }

    public int getTotalSiswaShowed()
    {
        return Integer.parseInt(totalStudents.getText());
    }
}
