package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:8000/")
public class LoginPage extends PageObject {

    @FindBy(xpath = "//input[@id='login-username']")
    WebElementFacade userNameInput;

    @FindBy(xpath = "//input[@id='login-password']")
    WebElementFacade passwordInput;

    @FindBy(xpath = "//input[@id='login']")
    WebElementFacade btnLogin;

    @FindBy(xpath = "//div[@class='logo text-center']")
    WebElementFacade logo;

    public void insertUsername(String userName)
    {
        userNameInput.sendKeys(userName);
    }

    public void insertPassword(String password)
    {
        passwordInput.sendKeys(password);
    }

    public void clickBtnLogin()
    {
        btnLogin.click();
    }

    public String getUsernameAppear()
    {
        return userNameInput.getAttribute("value");
    }

    public String getPasswordAppear()
    {
        return passwordInput.getAttribute("value");
    }

}
