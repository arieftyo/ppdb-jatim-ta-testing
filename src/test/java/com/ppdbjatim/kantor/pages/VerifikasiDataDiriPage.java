package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class VerifikasiDataDiriPage extends PageObject {
    @FindBy(xpath = "(//div[@class='card-body'])[1]//div[@class='col-md-9']")
    List<WebElementFacade> dataPribadi;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//button[@onclick='js_post_verifikasi_status_domisili(1)']")
    WebElementFacade btnSimpanDanVerifikasiDomisili;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//button[@class='btn btn-sm btn-success ml-1 float-right']")
    WebElementFacade btnSimpanPerubahanDomisili;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//button[@class='btn btn-sm btn-danger float-right']")
    WebElementFacade btnBatalkanPerubahanDomisili;

    @FindBy(xpath = "//select[@id='kota_siswa']")
    WebElementFacade selectKota;

    @FindBy(xpath = "//select[@id='kecamatan_peserta']")
    WebElementFacade selectKecamatan;

    @FindBy(xpath = "//input[@name='smp_kelurahan']")
    WebElementFacade inputKelurahan;

    @FindBy(xpath = "//input[@name='smp_rt_rw']")
    WebElementFacade inputRtRw;

    @FindBy(xpath = "//textarea[@name='smp_alamat_siswa_kk']")
    WebElementFacade inputAlamat;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//select[@id='kota_siswa_domisili']")
    WebElementFacade selectKotaDomisili;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//select[@id='kecamatan_peserta_domisili']")
    WebElementFacade selectKecamatanDomisili;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//input[@name='smp_kelurahan_domisili']")
    WebElementFacade inputKelurahanDomisili;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//input[@name='smp_rt_rw_domisili']")
    WebElementFacade inputRtRwDomisili;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//textarea[@name='smp_alamat_siswa_domisili']")
    WebElementFacade inputAlamatDomisili;

    @FindBy(xpath = "//input[@id='lat']")
    WebElementFacade inputLatitude;

    @FindBy(xpath = "//input[@id='lng']")
    WebElementFacade inputLongitude;

    @FindBy(xpath = "//div[@id='mapid']//img[@class='leaflet-marker-icon leaflet-zoom-animated leaflet-interactive leaflet-marker-draggable']")
    WebElementFacade pinPositionMap;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_longlat']//button[@onclick='js_post_verifikasi_status_longlat(1)']")
    WebElementFacade btnSimpanDanVerifikasiLatLong;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_longlat']//button[@id='simpanLokasi']")
    WebElementFacade btnSimpanPerubahanLatlong;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_longlat']//button[@class='btn btn-sm btn-danger float-right']")
    WebElementFacade btnBatalkanPerubahanLatlong;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_berkas']//input[@id='smp_nomor_kk']")
    WebElementFacade inputNomorKK;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_berkas']//input[@id='smp_nik']")
    WebElementFacade inputNIK;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_berkas']//button[@onclick='js_post_verifikasi_berkas(-1)']")
    WebElementFacade btnSalahBerkas;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_berkas']//button[@onclick='js_post_verifikasi_berkas(1)']")
    WebElementFacade btnBenarBerkas;

    @FindBy(xpath = "//form[@id='form-validasi']//button[@id='btn-confirm']")
    List<WebElementFacade> btnVerifikasiPeserta;

    @FindBy(xpath = "//form[@id='form-validasi']//button[@class='btn btn-danger']")
    WebElementFacade btnBerkasSalah;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_nilai']//button[@onclick='js_post_verifikasi_status_nilai(-1)']")
    WebElementFacade btnNilaiSalah;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_nilai']//button[@onclick='js_post_verifikasi_status_nilai(1)']")
    List<WebElementFacade> btnNilaiBenar;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_nilai']//button[@class='btn btn-sm btn-danger float-right' and @type='submit']")
    WebElementFacade btnBatalkanVerifikasiNilai;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_nilai']//button[@class='btn btn-sm btn-success ml-1 float-right' and @type='submit']")
    WebElementFacade btnBenarkanVerifikasiNilai;

    @FindBy(xpath = "//div[@id='verifModal']//div[@class='modal-content']//button[@id='btn-confirm']")
    WebElementFacade btnConfirmFinalVerifikasi;

    @FindBy(xpath = "//div[@class='container-fluid']//div[@class='col-lg-12 col-md-12 col-xs-12'][1]//div[@class='alert alert-success']")
    WebElementFacade alertSuccess;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_nilai']//label//div[@class='badge bg-success']")
    List<WebElementFacade> successNilai;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_nilai']//label//div[@class='badge bg-danger']")
    List<WebElementFacade> wrongNilai;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//label//div[@class='badge bg-success']")
    List<WebElementFacade> successDomisili;

    @FindBy(xpath = "//form[@id='form-post_verifikasi_status_domisili']//label//div[@class='badge bg-danger']")
    List<WebElementFacade> wrongDomisili;

    @FindBy(xpath = "//label[contains(text(), 'D. Lokasi Rumah Peserta | ')]//div[@class='badge bg-success']")
    List<WebElementFacade> successLocation;

    @FindBy(xpath = "//label[contains(text(), 'D. Lokasi Rumah Peserta | ')]//div[@class='badge bg-danger']")
    List<WebElementFacade> wrongLocation;

    @FindBy(xpath = "//label[contains(text(), 'E. Berkas Peserta | ')]//div[@class='badge bg-success']")
    List<WebElementFacade> successBerkas;

    @FindBy(xpath = "//label[contains(text(), 'E. Berkas Peserta | ')]//div[@class='badge bg-danger']")
    List<WebElementFacade> wrongBerkas;

    @FindBy(xpath = "//div[@class='card-footer text-right']//button[@class='btn btn-success']")
    List<WebElementFacade> successVerificationStudent;

    @FindBy(xpath = "//section[@class='forms']//div[@class='alert alert-danger']")
    List<WebElementFacade> rejectedMessage;

    public String getStudentName(){
        String elementText = dataPribadi.get(1).getText();
        String studentName = elementText.substring(elementText.lastIndexOf(":") + 2);
        return studentName;
    }

    public String getStudentNoUn()
    {
        String elementText = dataPribadi.get(0).getText();
        String studentNoUn = elementText.substring(elementText.lastIndexOf(":") + 2);
        return studentNoUn;
    }

    public void clickVerifikasiNilaiBenar()
    {
        if (btnNilaiBenar.size() != 0)
            btnNilaiBenar.get(0).click();
    }

    public void clickVerifikasiNilaiSalah()
    {
        btnNilaiSalah.click();
    }

    public void clickVerifikasiDomisili()
    {
        btnSimpanDanVerifikasiDomisili.click();
    }

    public void clickVerifikasiWrongDomisili()
    {
        btnBatalkanPerubahanDomisili.click();
    }

    public void clickVerifikasiLatLong()
    {
        btnSimpanDanVerifikasiLatLong.click();
    }

    public void clickVerifikasiWrongLatlong()
    {
        btnBatalkanPerubahanLatlong.click();
    }
    public void clickVerifikasiBerkas()
    {
        btnBenarBerkas.click();
    }

    public void clickVerifikasiWrongBerkas()
    {
        btnSalahBerkas.click();
    }

    public void clickOkInAlert()
    {
        WebDriverWait wait = new WebDriverWait(getDriver(), 2);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        alert.accept();
    }

    public void clickFinalVerificartion()
    {
        btnVerifikasiPeserta.get(0).click();
    }

    public void clickConfirmInFinalVerifikasi()
    {
        btnConfirmFinalVerifikasi.click();
    }

    public String getAlertSuccess()
    {
        return alertSuccess.getText();
    }

    public boolean isDataNilaiVerificationSuccess()
    {
        return successNilai.size() != 0;
    }

    public boolean isDataNilaiVerificationWrong()
    {
        return wrongNilai.size() != 0;
    }

    public boolean isDataDomisiliVerificationSuccess()
    {
        return successDomisili.size() != 0 ;
    }

    public boolean isDataDomisiliVerificationWrong(){
        return wrongDomisili.size() != 0;
    }

    public boolean isDataLocationVerificationSuccess()
    {
        return successLocation.size() != 0;
    }

    public boolean isDataLocationVerificationWrong()
    {
        return wrongLocation.size() != 0;
    }

    public boolean isDataBerkasVerificationSuccess()
    {
        return successBerkas.size() != 0;
    }

    public boolean isDataBerkasVerificationWrong()
    {
        return wrongBerkas.size() != 0;
    }

    public boolean isButtonVerificationStudentExist()
    {
        return btnVerifikasiPeserta.size() != 0;
    }

    public boolean isStatusSuccessVerificationStudentExist()
    {
        return successVerificationStudent.size() != 0;
    }

    public boolean isRejectedMessageShowed()
    {
        return rejectedMessage.size() != 0;
    }
}
