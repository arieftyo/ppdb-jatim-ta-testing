package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class DashboardPage extends PageObject {
    @FindBy(xpath = "//header[@class='page-header']//h2")
    WebElementFacade dashboardTitle;

    @FindBy(xpath = "//div[@class='sidebar-header d-flex align-items-center']//h1")
    WebElementFacade accountName;

    @FindBy (xpath = "//li//a[contains(@href, 'lihat_pagu')]")
    WebElementFacade lihatPaguMenuBtn;

    public String getDashboardTitle()
    {
        return dashboardTitle.getText();
    }

    public String getAccountName()
    {
        return accountName.getText();
    }

    public void clickBtnLihatPagu()
    {
        lihatPaguMenuBtn.click();
    }
}
