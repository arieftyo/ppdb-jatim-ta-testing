package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class EditKelolaPaguPage extends PageObject {
    @FindBy(xpath = "//header[@class='page-header']//h2")
    WebElementFacade editKelolaPageSMATitle;

    @FindBy(xpath = "//section[@class='forms']//div[@class='card']//*[@class='h4']")
    WebElementFacade editKelolaPageSMASubTitle;

    @FindBy(xpath = "//form[@id='pagu-form']//input[@id='rombel_12']")
    WebElementFacade inputFormRombelClass12;

    @FindBy(xpath = "//form[@id='pagu-form']//input[@id='rombel_11']")
    WebElementFacade inputFormRombelClass11;

    @FindBy(xpath = "//form[@id='pagu-form']//input[@id='rombel_10']")
    WebElementFacade inputFormRombelClass10;

    @FindBy(xpath = "//form[@id='pagu-form']//input[@id='siswa_rombel']")
    WebElementFacade inputSiswaRombel;

    @FindBy(xpath = "//form[@id='pagu-form']//button[@name='close']")
    WebElementFacade btnCancel;

    @FindBy(xpath = "//form[@id='pagu-form']//button[@id='btn-submit']")
    WebElementFacade btnSave;

    @FindBy(xpath = "//div[@id='swal2-content']")
    WebElementFacade modalWarningMaxSiswaPerRombel;

    public String getKelolaPageSMATitle()
    {
        return editKelolaPageSMATitle.getText();
    }

    public String getKelolaPageSMASubTitle()
    {
        return editKelolaPageSMASubTitle.getText();
    }

    public void editRombelClass12(String rombel)
    {
        inputFormRombelClass12.clear();
        inputFormRombelClass12.sendKeys(rombel);
    }

    public void editRombelClass11(String rombel)
    {
        inputFormRombelClass11.clear();
        inputFormRombelClass11.sendKeys(rombel);
    }

    public void editRombelClass10(String rombel)
    {
        inputFormRombelClass10.clear();
        inputFormRombelClass10.sendKeys(rombel);
    }

    public void editSiswaRombel(String rombel)
    {
        inputSiswaRombel.clear();
        inputSiswaRombel.sendKeys(rombel);
    }

    public String getRombelClass12()
    {
        return inputFormRombelClass12.getAttribute("value");
    }

    public String getRombelClass11()
    {
        return inputFormRombelClass11.getAttribute("value");
    }

    public String getRombelClass10()
    {
        return inputFormRombelClass10.getAttribute("value");
    }

    public String getsiswaPerRombel()
    {
        return inputSiswaRombel.getAttribute("value");
    }

    public String checkAndChangeRombel(int level)
    {
        String rombel = "6";
        if (level == 12)
        {
            if(inputFormRombelClass12.getAttribute("value").equals("7"))
            {
                rombel = "8";
            }
            else
            {
                rombel = "7";
            }
        }
        else if (level == 11)
        {
            if(inputFormRombelClass11.getAttribute("value").equals("7"))
            {
                rombel = "8";
            }
            else
            {
                rombel = "7";
            }
        }
        else if (level == 10)
        {
            if(inputFormRombelClass10.getAttribute("value").equals("7"))
            {
                rombel = "8";
            }
            else
            {
                rombel = "7";
            }
        }
        return rombel;
    }

    public String checkAndChangeSiswaRombel()
    {
        String rombel;
        if(inputSiswaRombel.getAttribute("value").equals("36"))
            rombel = "35";
        else
            rombel = "36";
        return rombel;
    }

    public String getRombel()
    {
        return inputFormRombelClass10.getAttribute("value");
    }

    public void clickBtnSaveEditPagu()
    {
        btnSave.click();
    }

    public void clickBtnCancelEditPagu()
    {
        btnCancel.click();
    }

    public boolean isModalWarningMaxSiswaPerRombelDisplayed()
    {
        return modalWarningMaxSiswaPerRombel.isDisplayed();
    }
}
