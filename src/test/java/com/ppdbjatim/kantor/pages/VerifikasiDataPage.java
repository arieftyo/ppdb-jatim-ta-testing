package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class VerifikasiDataPage extends PageObject {
    @FindBy(xpath = "(//a[contains(@href,'verifikasidata')])[1]")
    WebElementFacade btnMenuVerifikasiData;

    @FindBy(xpath = "//header//h2")
    WebElementFacade pageTitle;

    @FindBy(xpath = "//div[@class='card']//h3")
    WebElementFacade tableTitle;

    @FindBy(xpath = "//div[@class='title']//div[@class='number']//strong")
    WebElementFacade totalStudent;

    @FindBy(xpath = "//div[@class='table-responsive']//table[@id='table-validasi-offline']//tbody//tr")
    List<WebElementFacade> datasInTable;

    @FindBy(xpath = "//div[@class='table-responsive']//table[@id='table-validasi-offline']//tbody//tr//td[1]")
    List<WebElementFacade> dataNoUnTable;

    @FindBy(xpath = "//div[@class='table-responsive']//table[@id='table-validasi-offline']//tbody//tr//td[2]")
    List<WebElementFacade> dataNamaTable;

    @FindBy(xpath = "//div[@class='table-responsive']//table[@id='table-validasi-offline']//tbody//tr//td[6]//a")
    List<WebElementFacade> btnCekBerkasTable;

    @FindBy(xpath = "//div[@id='table-pagu_paginate']//span//a")
    List<WebElementFacade> pagination;

    @FindBy(xpath = "//a[@id='table-validasi-offline_next']")
    WebElementFacade btnNextPage;

    @FindBy(xpath = "//a[@class='paginate_button next disabled']")
    List<WebElementFacade> btnNextPageDisabled;

    @FindBy(xpath = "//div[@class='row']//div[@class='col-lg-12 col-md-12 col-xs-12'][1]//div[@class='alert alert-success']")
    WebElementFacade alertSuccessVerification;

    public void clickBtnMenuVerifikasiData()
    {
        btnMenuVerifikasiData.click();
    }

    public String getTableTitle()
    {
        return tableTitle.getText();
    }

    public String getPageTitle()
    {
        return pageTitle.getText();
    }

    public String getTotalStudent()
    {
        return totalStudent.getText();
    }

    public int getTotalTableDataStudent()
    {
        int total = 0;
        while (true)
        {
            total = total + datasInTable.size();
            if (btnNextPageDisabled.size() != 0)
            {
                break;
            }
            btnNextPage.click();
        }
        return total;
    }

    public void clickBtnCekBerkas(int index)
    {
        btnCekBerkasTable.get(index).click();
    }

    public String getNameStudentData(int index)
    {
        return dataNamaTable.get(index).getText();
    }

    public String getFirstNameStudentData()
    {
        return getNameStudentData(0);
    }

    public String getNoUnStudentData(int index)
    {
        return dataNoUnTable.get(index).getText();
    }

    public String getFirstNoUNStudentData()
    {
        return getNoUnStudentData(0);
    }

    public void clickFirstStudentCekBerkas()
    {
        Actions actions = new Actions(getDriver());
        actions.keyDown(Keys.LEFT_CONTROL)
                .click( btnCekBerkasTable.get(0))
                .keyUp(Keys.LEFT_CONTROL)
                .build()
                .perform();
    }

    public String getAlertSuccessVerification()
    {
        return alertSuccessVerification.getText();
    }

    public boolean isStudentInTableVerification(String noUn)
    {
        while (true)
        {
            for (WebElementFacade webElementFacade : dataNoUnTable) {
                if (webElementFacade.getText().equalsIgnoreCase(noUn)) {
                    return true;
                }
            }
            if (btnNextPageDisabled.size() != 0)
            {
                break;
            }
            btnNextPage.click();
        }
        return false;
    }

    public void clickCekBerkasWithNoUn(String noUn)
    {
        int i;
        Actions actions = new Actions(getDriver());
        while (true)
        {
            i = 0;
            for (WebElementFacade webElementFacade : dataNoUnTable) {
                if (webElementFacade.getText().equalsIgnoreCase(noUn)) {
                    actions.keyDown(Keys.LEFT_CONTROL)
                            .click( btnCekBerkasTable.get(i))
                            .keyUp(Keys.LEFT_CONTROL)
                            .build()
                            .perform();
                    break;
                }
                i++;
            }
            if (btnNextPageDisabled.size() != 0)
            {
                break;
            }
            btnNextPage.click();
        }
    }

}
