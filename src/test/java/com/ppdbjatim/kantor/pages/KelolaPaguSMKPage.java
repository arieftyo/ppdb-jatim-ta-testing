package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class KelolaPaguSMKPage extends PageObject {

    @FindBy(xpath = "//select[@name='kode_sekolah']")
    WebElementFacade selectSekolah;

    @FindBy(xpath = "(//div[@class='col-lg-12 col-md-12 col-xs-12'])[3]//h3")
    WebElementFacade schoolTitle;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[1]")
    List<WebElementFacade> schoolDataName;

    @FindBy(xpath = "//div[@class='card-footer']//div[@class='text-right']//a")
    WebElementFacade btnEditRombelClass;

    @FindBy(xpath = "//input[@id='rombel_12']")
    WebElementFacade inputRombelClass12;

    @FindBy(xpath = "//input[@id='rombel_11']")
    WebElementFacade inputRombelClass11;

    @FindBy(xpath = "//div[@class='text-right']//button[@id='btn-submit']")
    WebElementFacade btnSaveEditRombel;

    @FindBy(xpath = "//div[@class='text-right']//a//button")
    WebElementFacade btnCancelEditRombel;

    @FindBy(xpath = "(//div[@class='row text-center']//div//h1)[3]")
    WebElementFacade rombelClass12;

    @FindBy(xpath = "(//div[@class='row text-center']//div//h1)[2]")
    WebElementFacade rombelClass11;

    @FindBy(xpath = "(//div[@class='row text-center']//div//h1)[1]")
    WebElementFacade rombelClass10;

    @FindBy(xpath = "(//div[@class='text']//strong)[1]")
    WebElementFacade totalRombelClass;

    @FindBy(xpath = "//div[@id='swal2-content']")
    WebElementFacade warningModalMaximalRombel;

    @FindBy(xpath = "//div[@id='table-pagu_paginate']//span//a")
    List<WebElementFacade> pagination;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[2]")
    List<WebElementFacade> dataTableJurusan;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[3]")
    List<WebElementFacade> dataTableTenthClassRombel;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[4]")
    List<WebElementFacade> dataTableSiswaPerRombel;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[5]")
    List<WebElementFacade> dataTablePaguAwal;

    @FindBy(xpath = "(//div[@class='text']//strong)[2]")
    WebElementFacade totalPaguAwal;

    @FindBy(xpath = "//form[@class='form-hapus-jurusan']//button")
    List<WebElementFacade> btnDeleteJurusan;

    @FindBy(xpath = "//div[@class='swal2-actions']//button[@class='swal2-confirm swal2-styled']")
    WebElementFacade btnYesConfirmDelete;

    @FindBy(xpath = "//div[@class='alert alert-success']")
    WebElementFacade successAlert;

    @FindBy(xpath = "//div[@class='card-body']//div[@class='text-right']//button")
    WebElementFacade btnAddPaketKeahlian;

    @FindBy(xpath = "//select[@id='bidang']")
    WebElementFacade selectBidangKeahlian;

    @FindBy(xpath = "//select[@id='paket']")
    WebElementFacade selectPaketKeahlian;

    @FindBy(xpath = "//input[@id='rombel']")
    WebElementFacade inputJurusanRombel;

    @FindBy(xpath = "//input[@id='siswa_rombel']")
    WebElementFacade inputJurusanSiswaPerRombel;

    @FindBy(xpath = "//div[@class='modal-footer']//button[@type='submit']")
    WebElementFacade btnSaveAddJurusan;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[6]//a")
    List<WebElementFacade> btnEditJurusan;

    @FindBy(xpath = "//input[@id='rombel_10']")
    WebElementFacade inputRombelClass10;

    public void clickSelectFirstSchool()
    {
        Select school = new Select(selectSekolah);
        school.selectByIndex(1);
    }

    public String getSelectedSchool()
    {
        Select school = new Select(selectSekolah);
        return school.getFirstSelectedOption().getText();
    }

    public String getSchoolTitle()
    {
        return schoolTitle.getText();
    }

    public List<String> getSchoolDataName()
    {
        List<String> nameList = new ArrayList<>();
        for (WebElementFacade webElementFacade : schoolDataName) {
            nameList.add(webElementFacade.getText());
        }
        return nameList;
    }

    public void clickBtnEditRombelClass()
    {
        btnEditRombelClass.click();
    }

    public void editRombelClass12SMK(String rombel)
    {
        inputRombelClass12.clear();
        inputRombelClass12.sendKeys(rombel);
    }

    public void editRombelClass11SMK(String rombel)
    {
        inputRombelClass11.clear();
        inputRombelClass11.sendKeys(rombel);
    }

    public String getInputRombelClass12SMK()
    {
        return inputRombelClass12.getAttribute("value");
    }

    public String getInputRombelClass11SMK()
    {
        return inputRombelClass11.getAttribute("value");
    }

    public String getInputRombelClass10SMK()
    {
        return inputRombelClass10.getAttribute("value");
    }

    public String getInputSiswaPerRombelSMK()
    {
        return inputJurusanSiswaPerRombel.getAttribute("value");
    }

    public String checkAndChangeSiswaRombelSMK(String rombelNow)
    {
        String rombel;
        if(rombelNow.equals("24"))
            return "23";
        else
            return "24";
    }

    public void clickBtnSaveEditRombel()
    {
        btnSaveEditRombel.click();
    }

    public void clickBtnCancelEditRombel()
    {
        btnCancelEditRombel.click();
    }

    public String getRombelClass12()
    {
        return rombelClass12.getText();
    }

    public String getRombelClass11()
    {
        return rombelClass11.getText();
    }

    public String getRombleClass10() { return rombelClass10.getText();}

    public String getTotalRombelClass()
    {
        return totalRombelClass.getText();
    }

    public int getTotalRombelFromData()
    {
        int rombel12 = Integer.parseInt(getRombelClass12());
        int rombel11 = Integer.parseInt(getRombelClass11());
        int rombel10 = Integer.parseInt(getRombleClass10());

        int totalRombel = rombel10 + rombel11 + rombel12;
        return totalRombel;
    }

    public boolean isWarningMaximalRombelDisplayed()
    {
        return warningModalMaximalRombel.isDisplayed();
    }

    public int getTotalDataTable(List<WebElementFacade> listWebElement)
    {
        int totalData = 0;
        int i = 0;
        while (i < pagination.size())
        {
            pagination.get(i).click();
            for (WebElementFacade data : listWebElement) {
                totalData = totalData + Integer.parseInt(data.getText());
            }
            i++;
        }
        return totalData;
    }
    public int getTotalDataTableRombelTenthClass()
    {
       return getTotalDataTable(dataTableTenthClassRombel);
    }

    public int getTotalDataTablePaguAwal()
    {
        return getTotalDataTable(dataTablePaguAwal);
    }

    public int getTotalPaguAwal()
    {
        return Integer.parseInt(totalPaguAwal.getText());
    }

    public int getMultiplicationRombelWithSiswaPerRombel(int index)
    {
        int rombel = Integer.parseInt(dataTableTenthClassRombel.get(index).getText());
        int siswaPerRombel = Integer.parseInt(dataTableSiswaPerRombel.get(index).getText());
        return rombel * siswaPerRombel;
    }

    public boolean isDataPaguAwalTrue()
    {
        int i = 0;
        int index = 0;
        int paguAwal;
        while (i < pagination.size())
        {
            pagination.get(i).click();
            index = 0;
            for (WebElementFacade data : dataTablePaguAwal)
            {
                paguAwal = Integer.parseInt(data.getText());
                if (getMultiplicationRombelWithSiswaPerRombel(index) != paguAwal)
                    return false;
                index++;
            }
            i++;
        }
        return true;
    }

    public void deleteJurusan(int index)
    {
        btnDeleteJurusan.get(index).click();
        btnYesConfirmDelete.click();
        btnYesConfirmDelete.click();
    }

    public void deleteFirstJurusan()
    {
        deleteJurusan(0);
    }

    public int getIndexJuruan(String jurusan)
    {
        int i = 0 ;
        for (WebElementFacade dataJurusan : dataTableJurusan)
        {
            if ( dataJurusan.getText().equalsIgnoreCase(jurusan) )
                return i;
            i++;
        }

        return i;
    }

    public String getFirstJurusan()
    {
        return dataTableJurusan.get(0).getText();
    }

    public String getJurusanByIndex(int index)
    {
        return dataTableJurusan.get(index).getText();
    }

    public boolean isSuccessAlertDisplayed()
    {
        return successAlert.isDisplayed();
    }

    public String getSuccessAlert()
    {
        return successAlert.getText();
    }

    public void deleteJurusanByJurusanName(String jurusan)
    {
        int i = 0;
        int index = 0;
        while (i < pagination.size())
        {
            pagination.get(i).click();
            index = 0;
            for(WebElementFacade dataJurusan : dataTableJurusan)
            {
                if (dataJurusan.getText().equalsIgnoreCase(jurusan))
                {
                    deleteJurusan(index);
                }
                index++;
            }
            i++;
        }
    }

    public boolean isJurusanExist(String jurusan)
    {
        int i = 0;
        int index = 0;
        while (i < pagination.size())
        {
            pagination.get(i).click();
            index = 0;
            for(WebElementFacade dataJurusan : dataTableJurusan)
            {
                if (dataJurusan.getText().equals(jurusan))
                {
                    return true;
                }
                index++;
            }
            i++;
        }
        return false;
    }

    public void clickBtnAddPaketKeahlian()
    {
        btnAddPaketKeahlian.click();
    }

    public void clickSaveAddJurusan()
    {
        btnSaveAddJurusan.click();
    }

    public void clickSelectBidangKeahlianByName(String bidangKeahlian)
    {
        Select bidangSelect = new Select(selectBidangKeahlian);
        bidangSelect.selectByVisibleText(bidangKeahlian);
    }

    public void clickSelectPaketKeahlianByName(String paketKeahlian)
    {
        Select paketSelect = new Select(selectPaketKeahlian);
        paketSelect.selectByVisibleText(paketKeahlian);
    }

    public void addJurusanRombel(String jurusanRombel)
    {
        inputJurusanRombel.sendKeys(jurusanRombel);
    }

    public void addJurusanSiswaPerRombel(String siswaPerRombel)
    {
        inputJurusanSiswaPerRombel.sendKeys(siswaPerRombel);
    }

    public void clickFirstBtnEditJurusan()
    {
        btnEditJurusan.get(0).click();
    }

    public String changeSiswaTenthClassRombelSMK()
    {
        String rombel;
        if(inputJurusanSiswaPerRombel.getAttribute("value").equals("36"))
            rombel = "35";
        else
            rombel = "36";
        return rombel;
    }

    public String checkAndChangeRombelSMK()
    {
        String rombel;
        if(inputRombelClass10.getAttribute("value").equals("2"))
            rombel = "1";
        else
            rombel = "2";
        return rombel;
    }

    public void editRombelClass10SMK(String rombel)
    {
        inputRombelClass10.clear();
        inputRombelClass10.sendKeys(rombel);
    }

    public void editSiswaRombelJurusanSMK(String total)
    {
        inputJurusanSiswaPerRombel.clear();
        inputJurusanSiswaPerRombel.sendKeys(total);
    }

    public String getDataFirstJurusanRombel()
    {
        return dataTableTenthClassRombel.get(0).getText();
    }

    public String getDataFirstJurusanSiswaPerRombel()
    {
        return dataTableSiswaPerRombel.get(0).getText();
    }

}
