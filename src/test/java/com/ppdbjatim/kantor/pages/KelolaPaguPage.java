package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class KelolaPaguPage extends PageObject {

    @FindBy(xpath = "//a[contains(@href, '#pagudropdown')]")
    WebElementFacade menuKelolaPagu;

    @FindBy(xpath ="(//ul[@id='pagudropdown']//a[contains(@href, 'pagu/sma')])[1]" )
    WebElementFacade menuPaguSMA;

    @FindBy(xpath = "(//ul[@id='pagudropdown']//a[contains(@href, 'pagu/smk')])[1]")
    WebElementFacade menuPaguSMK;

    @FindBy(xpath = "//ul[@id='pagudropdown']//a[contains(@href, 'pagu/sma_tidak_naik_kelas')]")
    WebElementFacade menuPaguSMATidakNaik;

    @FindBy(xpath = "//ul[@id='pagudropdown']//a[contains(@href, 'pagu/smk_tidak_naik_kelas')]")
    WebElementFacade menuPaguSMKTidakNaik;

    @FindBy(xpath = "//header[@class='page-header']//h2")
    WebElementFacade kelolaPaguPageTitle;

    @FindBy(xpath = "//select[@name='kode_kota_kabupaten']")
    WebElementFacade selectKotaKabupatenPagu;

    @FindBy(xpath = "//table[@id='table-pagu']//a[@class='btn btn-primary pull-left']")
    List<WebElementFacade> btnEditPaguInSchool;

    @FindBy(xpath = "(//table[@id='table-pagu']//tbody//tr)[1]//td")
    List<WebElementFacade> firstRowInDataTable;

    public String getKelolaPaguTitle()
    {
        return kelolaPaguPageTitle.getText();
    }

    public void clickMenuKelolaPagu()
    {
        menuKelolaPagu.click();
    }

    public void clickMenuPaguSMA()
    {
        menuPaguSMA.click();
    }

    public void clickMenuPaguSMK()
    {
        menuPaguSMK.click();
    }

    public void clickMenuPaguSMATidakNaik() {
        menuPaguSMATidakNaik.click();
    }

    public void clickMenuPaguSMKTidakNaik()
    {
        menuPaguSMKTidakNaik.click();
    }

    public void clickSelectKotaKabupaten(String namaKota)
    {
        Select kotaKabupaten = new Select(selectKotaKabupatenPagu);
        kotaKabupaten.selectByVisibleText(namaKota);
    }

    public String getSelectedKotaKabupaten()
    {
        Select kotaKabupaten = new Select(selectKotaKabupatenPagu);
        return kotaKabupaten.getFirstSelectedOption().getText();
    }

    public void clickButtonEditPaguInFirstSchool()
    {
        btnEditPaguInSchool.get(0).click();
    }

    public String getFirstDataRombelClass12()
    {
        return firstRowInDataTable.get(1).getText();
    }

    public  String getFirstDataRombelClass11()
    {
        return firstRowInDataTable.get(2).getText();
    }

    public String getFirstDataRombelClass10()
    {
        return firstRowInDataTable.get(3).getText();
    }

    public String getFirstDataSiswaRombel()
    {
        return firstRowInDataTable.get(4).getText();
    }

    public String getDataPaguAwal()
    {
        return  firstRowInDataTable.get(5).getText();
    }
}
