package com.ppdbjatim.kantor.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class KelolaPaguSMKTidakNaikPage extends PageObject {
    @FindBy(xpath = "//select[@name='kode_sekolah']")
    WebElementFacade selectSekolah;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[3]")
    List<WebElementFacade> paguAwal;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td[4]")
    List<WebElementFacade> paguTidakNaik;

    @FindBy(xpath = "//table[@id='table-pagu']//tbody//tr//td//button")
    List<WebElementFacade> btnEditPaguTidakNaik;

    @FindBy(xpath = "//input[@name='pagu_tidak_naik_kelas']")
    List<WebElementFacade> inputPaguTidakNaik;

    @FindBy(xpath = "//button[@id='data_peserta']")
    List<WebElementFacade> btnSavePaguTidakNaik;

    public void clickSelectFirstSchool()
    {
        Select school = new Select(selectSekolah);
        school.selectByIndex(1);
    }

    public String getSelectedSchool()
    {
        Select school = new Select(selectSekolah);
        return school.getFirstSelectedOption().getText();
    }

    public String getFirstSchoolPaguAwal()
    {
        return paguAwal.get(0).getText();
    }

    public String getFirstSchoolPaguTidakNaik()
    {
        return paguTidakNaik.get(0).getText();
    }

    public void clickFirstSchoolEditPaguTidakNaik()
    {
        btnEditPaguTidakNaik.get(0).click();
    }

    public String getFirstInputPaguTidakNaik()
    {
        return inputPaguTidakNaik.get(0).getAttribute("value");
    }

    public void changeFirstPaguTidakNaik(String pagu)
    {
        inputPaguTidakNaik.get(0).clear();
        inputPaguTidakNaik.get(0).sendKeys(pagu);
    }

    public String checkPaguTidakNaik()
    {
        if (getFirstInputPaguTidakNaik().equalsIgnoreCase("2"))
            return "1";
        else
            return "2";
    }

    public void clickFirstBtnSsvePaguTidakNaik()
    {
        btnSavePaguTidakNaik.get(0).click();
    }
}
