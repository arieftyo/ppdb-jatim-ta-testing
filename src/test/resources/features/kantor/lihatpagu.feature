Feature: Verify feature in lihat pagu page Kantor PPDB Jatim 2020

  Background: User open kantor and login Admin
    Given user in kantor ppdbjatim login page
    And user insert username Admin
    And user insert password Admin
    When user click login button

  @TestLihatPagu @Regression
  Scenario: User click menu lihat pagu in sidebar
    Given user click menu lihat pagu
    Then user should be in lihat pagu page

  @TestLihatPagu @Regression
  Scenario: User select city in lihat pagu page
    Given user click menu lihat pagu
    When user select one city in lihat pagu page
    Then data pagu should be corresponding with the city