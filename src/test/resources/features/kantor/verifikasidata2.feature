Feature: Verify feature to verify student uploaded data in Kantor PPDB Jatim 2020 - 2

  Background: User open kantor and login User Sekolah
    Given user in kantor ppdbjatim login page
    And user insert username sekolah-2
    And user insert password sekolah-2
    When user click login button

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification nilai true
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - nilai
    And move to student verifikasi data page
    When user verify student data's nilai
    Then student data's nilai is verificated

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification nilai wrong
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - nilai wrong
    And move to student verifikasi data page
    When user click wrong verify student data's nilai
    Then student data's nilai is wrong verificated

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification domisili true
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - domisili
    And move to student verifikasi data page
    When user verify student data's domisili
    Then student data's domisili is verificated

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification domisili wrong
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - domisili wrong
    And move to student verifikasi data page
    When user verify student data's domisili
    And user click wrong verify student data's domisili
    Then student data's domisili is wrong verificated

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification location true
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - location
    And move to student verifikasi data page
    When user verify student data's location
    Then student data's location is verificated

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification location wrong
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - location wrong
    And move to student verifikasi data page
    When user verify student data's location
    And user click wrong verify student data's location
    Then student data's location is wrong verificated

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification berkas true
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - berkas
    And move to student verifikasi data page
    When user verify student data's berkas
    Then student data's berkas is verificated

  @TestVerifikasiData @Regression
  Scenario: Verify feature verification berkas true
    Given user click menu verifikasi data
    And user click student in verifikasi data with nomor UN - berkas wrong
    And move to student verifikasi data page
    When user click wrong verify student data's berkas
    Then student data's berkas is wrong verificated