Feature: Verify feature to monitoring data verification in Kantor PPDB Jatim 2020

  Background: User open kantor and login User Cabang Dinas
    Given user in kantor ppdbjatim login page
    And user insert username cabang dinas
    And user insert password cabang dinas
    When user click login button

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify User click menu monitoring data
    Given user click menu monitoring data verification
    Then user should be in monitoring data verification Page

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify user insert no Un in monitoring verification searching
    Given user click menu monitoring data verification
    When user insert no Un in searching
    And user click button search
    Then data result no Un should be same with input

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify PIN in monitor data student wait for verification
    Given user click menu monitoring data verification
    When user insert no Un student wait for verification in searching
    And user click button search
    Then  data PIN should not exist
    And data status is wait for verification

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify check button verification data student wait for verification
    Given user click menu monitoring data verification
    And user insert no Un student wait for verification in searching
    And user click button search
    When user click button cek berkas in student data
    And move to student verifikasi data page
    Then button verification student is still exist

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify PIN in monitor data student finish verification
    Given user click menu monitoring data verification
    When user insert no Un student finish verification
    And user click button search
    Then data PIN shold be exist
    And data status is finish verification

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify check button verification data student finish verification
    Given user click menu monitoring data verification
    And user insert no Un student finish verification
    And user click button search
    When user click button cek berkas in student data
    And move to student verifikasi data page
    Then button verification student is not exist
    And page show status student is already verificated

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify PIN in monitor data student rejected verification
    Given user click menu monitoring data verification
    When user insert no Un student rejected verification
    And user click button search
    Then data PIN should not exist
    And data status is rejected verification

  @TestMonitoringVerifikasi @Regression
  Scenario: Verify check button verification data student rejected
    Given user click menu monitoring data verification
    And user insert no Un student rejected verification
    And user click button search
    When user click button cek berkas in student data
    And move to student verifikasi data page
    Then button verification student is still exist
    And page show rejected message

