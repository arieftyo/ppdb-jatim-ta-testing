Feature: Verify feature to verify student uploaded data in Kantor PPDB Jatim 2020

  Background: User open kantor and login User Sekolah
    Given user in kantor ppdbjatim login page
    And user insert username sekolah
    And user insert password sekolah
    When user click login button

  @TestVerifikasiData @Regression
  Scenario: Verify User click menu verifikasi data
    Given user click menu verifikasi data
    Then user should be in verifikasi data Page

  @TestVerifikasiData @Regression
  Scenario: Verify total number of student in verifikasi data
    Given user click menu verifikasi data
    Then total student that is showed must be true

  @TestVerifikasiData @Regression
  Scenario: Verify click one student in list of verifikasi data
    Given user click menu verifikasi data
    And set first verifikasi student data
    When user click first student in verifikasi data
    And move to student verifikasi data page
    Then user in verifikasi data calon peserta page must be true

  @TestVerifikasiData @Regression
  Scenario: Verify the student data's verification in verifikasi data
    Given user click menu verifikasi data
    And user click first student in verifikasi data
    And move to student verifikasi data page
    And set student personal data
    When user verify student data's nilai
    And user verify student data's domisili
    And user verify student data's location
    And user verify student data's berkas
    And user give final verification to student data
    Then student data must be verificated
    When user click menu daftar siswa selesai diverifikasi
    Then student data is showed in success verification page

  @TestVerifikasiData @Regression
  Scenario: Verify total number of student in selesai verifikasi data
    Given user click menu daftar siswa selesai diverifikasi
    When user click last pagination table button
    Then total student in daftar siswa selesai verifikasi must be true