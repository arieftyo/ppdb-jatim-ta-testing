Feature: Verify feature to verify healt data of student in Kantor PPDB Jatim 2020 - 2

  Background: User open kantor and login User Sekolah
    Given user in kantor ppdbjatim login page
    And user insert username sekolah SMK
    And user insert password sekolah SMK
    When user click login button
    And user click menu verifikasi kesehatan

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify User click menu health verification
    Then user in verifikasi kesehatan page

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify total student in health verification
    Then total student in verifikasi kesehatan is true

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify click one student in health verification
    Given user set data student in health verification
    When user click cek berkas in student health data- not verificated
    And move to student health verification data page
    Then selected student data in health verification student must be true

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify check status health verification student not verificated
    Then information status student in health verification is wait for verification
    Given user click cek berkas in student health data- not verificated
    When move to student health verification data page
    Then button health verification is exist
    And button file wrong health verification is exist

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify check status health verification student verificated
    Then information status student in health verification is success for verification
    Given user click cek berkas in student health data- verificated
    When move to student health verification data page
    Then button health verification is not exist
    And button file wrong health verification is not exist
    And status verification done is showed

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify check status health verification student rejected verification
    Then information status student in health verification is wait for revision
    Given user click cek berkas in student health data- rejected
    When move to student health verification data page
    Then button health verification is not exist
    And button status wait for revision is showed disabled

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify accept student health verification
    Given user click cek berkas in student health data- not verificated
    And move to student health verification data page
    When change status info colour blind into positive colour blind
    And change status info body tall into 160
    And verify accept student health data
    Then status student is success health verification
    When user click cek berkas in student health data- not verificated
    And move to student health verification data page 2
    Then status info colour blind is changed same with positive
    And status body tall is changed same with 160
    And status verification done is showed
    And make student status data health verification into normal - not verificated

  @TestVerifikasiKesehatan @Regression
  Scenario: Verify reject student health verification
    Given user click cek berkas in student health data- not verificated rejected
    And move to student health verification data page
    When reject student health verification
    Then information status student in health verification is wait for revision - not verificated rejected
    When user click cek berkas in student health data- not verificated rejected
    And move to student health verification data page 2
    Then button health verification is not exist
    And button status wait for revision is showed disabled
    And message wrong file is same with input
    And make student status data health verification into normal - not verificated rejected


