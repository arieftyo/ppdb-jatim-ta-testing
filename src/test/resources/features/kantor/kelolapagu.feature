Feature: Verify feature in Kelola Pagu Page Kantor PPDB Jatim 2020

  Background: User open kantor and login Admin
    Given user in kantor ppdbjatim login page
    And user insert username Admin
    And user insert password Admin
    When user click login button

  @TestKelolaPagu @Regression
  Scenario Outline:Verify User click menu kelola pagu <Type>
    Given user click menu kelola pagu
    And user click menu kelola pagu <Type>
    Then user should be in kelola pagu page <Type>
    Examples:
      | Type                |
      | Pagu SMA            |
      | Pagu SMK            |
      | Pagu SMA Tidak Naik |
      | Pagu SMK Tidak Naik |

  @TestKelolaPagu @Regression
  Scenario Outline:Verify User Click city in kelola pagu page <Type>
    Given user click menu kelola pagu
    And user click menu kelola pagu <Type>
    When select one city in kelola pagu page
    Then data kelola pagu page should be corresponding with the city
    Examples:
    | Type                |
    | Pagu SMA            |
    | Pagu SMK            |
    | Pagu SMA Tidak Naik |
    | Pagu SMK Tidak Naik |

  @TestKelolaPaguSMA @Regression
  Scenario:Pagu SMA -  Verify Show data pagu awal in kelola pagu page SMA
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMA
    Then data pagu awal must be true

  @TestKelolaPaguSMA @Regression
  Scenario:Pagu SMA - Verify User click edit pagu button in kelola pagu page SMA
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMA
    When user click edit pagu button
    Then user should be in Edit Pagu SMA page

  @TestKelolaPaguSMA @Regression
  Scenario:Pagu SMA -Verify User change rombel in all class and total siswa each rombel
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMA
    And user click edit pagu button
    When user change rombel in 12th class
    And user change rombel in 11th class
    And user change rombel in 10th class
    And user change total siswa rombel
    And set data rombel and total siswa
    And user click save edit pagu button
    Then rombel and data siswa must same with data in kelola page

  @TestKelolaPaguSMA @Regression
  Scenario:Pagu SMA - Verify User change rombel in all class and total siswa each rombel but cancel to save it
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMA
    And user click edit pagu button
    When user change rombel in 12th class
    And user change rombel in 11th class
    And user change rombel in 10th class
    And user change total siswa rombel
    And set data rombel and total siswa
    And user click cancel edit pagu button
    Then rombel and data siswa must different with data in kelola page

  @TestKelolaPaguSMA @Regression
  Scenario: Pagu SMA - Verify Total siswa per rombel maximal is 36
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMA
    And user click edit pagu button
    When user change total siswa rombel into 37
    Then appear warning modal maximal siswa per rombel

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify User choose one SMK in one of the city
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    When user choose one school SMK
    Then school that is choosen must be true

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify User change class rombel in SMK
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    And user choose one school SMK
    And user click edit pagu button SMK
    When user change rombel in 12th class SMK
    And user change rombel in 11th class SMK
    And set data rombel SMK
    And user click save edit pagu button SMK
    Then rombel must be same with edited data SMK
    And total rombel must be true SMK

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify Total maximal rombel is 72
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    And user choose one school SMK
    And user click edit pagu button SMK
    When user change rombel in twelfth class SMK into 72
    Then appear warning modal maximal total rombel SMK

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify rombel tenth class is same with data in table
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    When user choose one school SMK
    Then total rombel tenth class must be same with data SMK

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify total all pagu awal tenth class
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    When user choose one school SMK
    Then total all pagu awal tenth class must be true SMK

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify data pagu awal are the result from multiplication rombel with total student
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    When user choose one school SMK
    Then data pagu awal are got from multiplication rombel with total student SMK

  @TestKelolaPaguSMK @Regression @Test
  Scenario: Pagu SMK - Verify delete jurusan in SMK
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    When user choose one school SMK
    And user delete one jurusan in SMK
    Then jurusan SMK must be deleted
    And insert jurusan again

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify add jurusan in SMK
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    And user choose one school SMK
    When delete jurusan if that exist
    And user add jurusan in SMK
    And user select bidang keahlian SMK
    And user select paket keahlian SMK
    And user input rombel in add jurusan SMK
    And user input siswa per rombel in add jurusan SMK
    And user click save add jurusan SMK
    Then jurusan should be added to SMK

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify edit data jurusan in SMK
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    And user choose one school SMK
    When user click edit button in first jurusan SMK
    And user change tenth class rombel in edit jurusan SMK
    And user change siswa per rombel in edit jurusan SMK
    When set data tenth class rombel and siswa per rombel
    And user click save edit pagu button SMK
    Then tenth class rombel and siswa per rombel must be changed

  @TestKelolaPaguSMK @Regression
  Scenario: Pagu SMK - Verify total maximal siswa per rombel is 36
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK
    And user choose one school SMK
    When user click edit button in first jurusan SMK
    And user change siswa per rombel in edit jurusan SMK into the exceeded value
    Then appear warning modal maximal total rombel SMK

  @TestKelolaPaguSMATidakNaik @Regression
  Scenario: Pagu SMA Tidak Naik - Verify edit data SMA Tidak Naik
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMA Tidak Naik
    When user click edit button in first school SMA Tidak Naik
    And user change pagu in SMA Tidak Naik
    And set data pagu in SMA Tidak Naik
    And user click save edit pagu SMA Tidak Naik
    Then data pagu tidak naik must be changed

  @TestKelolaPaguSMKTidakNaik @Regression
  Scenario: Pagu SMK Tidak Naik - Verify edit data SMK Tidak Naik
    Given user click menu kelola pagu
    And user click menu kelola pagu Pagu SMK Tidak Naik
    And user choose one school SMK Tidak Naik
    When user click edit button in first school SMK Tidak Naik
    And user change pagu in SMK Tidak Naik
    And set data pagu in SMK Tidak Naik
    And user click save edit pagu SMK Tidak Naik
    Then data pagu SMK tidak naik must be changed