Feature: Verify feature in login page Kantor PPDB Jatim 2020

  @TestLogin @Regression
  Scenario: User insert username
    Given user in kantor ppdbjatim login page
    When user insert username Admin
    Then username should be appear Admin
    And usernam should be same with the insertion Admin

  @TestLogin @Regression
  Scenario: User insert password
    Given user in kantor ppdbjatim login page
    When user insert password Admin
    Then password should be appeared Admin
    And password should be same with the insertion Admin

  @TestLogin @Regression
  Scenario: User login with valid username and password
    Given user in kantor ppdbjatim login page
    When user insert username Admin
    And user insert password Admin
    And user click login button
    Then user should be logged in and in dashboard page