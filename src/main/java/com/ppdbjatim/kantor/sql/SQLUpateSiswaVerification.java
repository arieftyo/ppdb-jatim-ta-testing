package com.ppdbjatim.kantor.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static com.ppdbjatim.kantor.sql.DatabaseConfiguration.*;

public class SQLUpateSiswaVerification {
    static Connection conn;
    static Statement stmt;
    static int rs;

    public void UpdateSiswaStatusBerkasIntoNormal(String noUn)
    {
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();

            // buat query ke database
            String sql = "UPDATE data_siswa_smp d SET d.smp_status_berkas = 0 WHERE d.smp_uasbn = '" + noUn + "'";

            // eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.executeUpdate(sql);

            // tampilkan hasil query
            if(rs != 0)
            {
                System.out.println("sukses");
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UpdateSiswaStatusNilaiIntoNormal(String noUn)
    {
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();

            // buat query ke database
            String sql = "UPDATE data_siswa_smp d SET d.smp_status_nilai = 0 WHERE d.smp_uasbn = '" + noUn + "'";

            // eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.executeUpdate(sql);

            // tampilkan hasil query
            if(rs != 0)
            {
                System.out.println("sukses");
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UpdateSiswaStatusDomisiliIntoNormal(String noUn)
    {
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();

            // buat query ke database
            String sql = "UPDATE data_siswa_smp d SET d.smp_status_domisili = 0 WHERE d.smp_uasbn = '" + noUn + "'";

            // eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.executeUpdate(sql);

            // tampilkan hasil query
            if(rs != 0)
            {
                System.out.println("sukses");
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UpdateSiswaStatusLonglatIntoNormal(String noUn)
    {
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();

            // buat query ke database
            String sql = "UPDATE data_siswa_smp d SET d.smp_status_longlat = 0 WHERE d.smp_uasbn = '" + noUn + "'";

            // eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.executeUpdate(sql);

            // tampilkan hasil query
            if(rs != 0)
            {
                System.out.println("sukses");
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
