package com.ppdbjatim.kantor.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static com.ppdbjatim.kantor.sql.DatabaseConfiguration.*;
import static com.ppdbjatim.kantor.sql.DatabaseConfiguration.PASS;

public class SQLUpdateSiswaHealthVerification {
    static Connection conn;
    static Statement stmt;
    static int rs;

    public void UpdateSiswaStatusHealthVerificationNormal(String noUn)
    {
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();

            // buat query ke database
            String sql = "UPDATE data_siswa_smp d SET d.smp_tes_buta_warna = 1, d.smp_tes_tinggi_badan = 156, " +
                    "d.smp_status_berkas_kesehatan = 0 WHERE d.smp_uasbn = '" + noUn + "';";

            // eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.executeUpdate(sql);

            // tampilkan hasil query
            if(rs != 0)
            {
                System.out.println("sukses");
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
