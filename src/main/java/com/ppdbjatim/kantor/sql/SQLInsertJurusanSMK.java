package com.ppdbjatim.kantor.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static com.ppdbjatim.kantor.sql.DatabaseConfiguration.*;
import static com.ppdbjatim.kantor.sql.DatabaseConfiguration.PASS;

public class SQLInsertJurusanSMK {
    static Connection conn;
    static Statement stmt;
    static boolean rs;

    public void InsertJurusanIntoSMK1Surabaya()
    {
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();

            // buat query ke database
            String sql = "INSERT INTO `sekolah` VALUES (5674, '20532205', '40501101001', 'SMK NEGERI 1 SURABAYA', 'TEKNIK KOMPUTER DAN JARINGAN', 66, 'JL. SMEA NO. 4 WONOKROMO SURABAYA', 'TJIPTOADI NUGROHO, S.ST, M.T.', '19650409 199802 1 001', NULL, 'K', NULL, NULL, NULL, NULL, NULL, 2, 36, 72, 4, NULL, 6, NULL, 3, 7, 4, 4, NULL, 1, 1, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 114, 76, NULL, 0, 0, 664, '0131', NULL, NULL, NULL, NULL, NULL, '-7.3052', '112.7339', NULL, 0, 45, 0, 0, 0, 45, NULL, NULL, NULL, NULL, 6, 44, 4);";

            // eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.execute(sql);

            // tampilkan hasil query
            if(rs)
            {
                System.out.println("sukses");
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
