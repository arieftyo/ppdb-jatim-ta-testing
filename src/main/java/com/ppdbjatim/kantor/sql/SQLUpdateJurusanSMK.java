package com.ppdbjatim.kantor.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static com.ppdbjatim.kantor.sql.DatabaseConfiguration.*;
import static com.ppdbjatim.kantor.sql.DatabaseConfiguration.PASS;

public class SQLUpdateJurusanSMK {

    static Connection conn;
    static Statement stmt;
    static int rs;

    public void UpdateJurusanIntoNormalSMK1Surabaya()
    {
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // buat objek statement
            stmt = conn.createStatement();

            // buat query ke database
            String sql = "UPDATE sekolah s SET s.rombel ='24', s.pagu_awal='864' WHERE s.kode_sekolah = '40501101000';";
            // eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.executeUpdate(sql);

            // tampilkan hasil query
            if(rs != 0)
            {
                System.out.println("sukses");
            }

            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
