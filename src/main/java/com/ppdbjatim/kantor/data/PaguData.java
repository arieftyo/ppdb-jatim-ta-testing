package com.ppdbjatim.kantor.data;

import lombok.Data;

@Data
public class PaguData {
    private String rombelClass12;
    private String rombelClass11;
    private String rombelClass10;
    private String siswaPerRombel;
    private String jurusan;
    private String paguTidakNaik;

    private static PaguData instance= null;
    public static PaguData getInstance()
    {
        if (instance == null){
            instance = new PaguData();
        }
        return instance;
    }

}
