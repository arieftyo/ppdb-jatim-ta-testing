package com.ppdbjatim.kantor.data;

import lombok.Data;

@Data
public class VerifikasiStudentData {
    private String name;
    private String noUn;

    private static VerifikasiStudentData instance= null;
    public static VerifikasiStudentData getInstance()
    {
        if (instance == null){
            instance = new VerifikasiStudentData();
        }
        return instance;
    }
}
