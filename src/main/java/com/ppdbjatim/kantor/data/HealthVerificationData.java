package com.ppdbjatim.kantor.data;

import lombok.Data;

@Data
public class HealthVerificationData {
    private String noUn;
    private String name;
    private String messageWrongFile;

    private static HealthVerificationData instance= null;
    public static HealthVerificationData getInstance()
    {
        if (instance == null){
            instance = new HealthVerificationData();
        }
        return instance;
    }
}
